from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import \
    OPEN_URL_FUNC, json
from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import ROOT_ENDPOINT
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.nvmedomains.nvme_domain import NVMeDomain


class TestNVMeDomain:
    def test_delete_nvme_domain(
            self,
            swordfish,
            make_mock,
            open_url_kwargs,
            nvme_domain_data
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[nvme_domain_data, nvme_domain_data],
            chain_calls=True
        )
        nvme_domain = swordfish.get_nvme_domain('test_domain')
        nvme_domain.delete_nvme_domain("test_domain")
        expected_url = "https://localhost{0}/NVMeDomains/{1}".format(ROOT_ENDPOINT, "test_domain")
        open_url_kwargs.update(
            method='DELETE',
            url=expected_url,
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_get_nvme_domain(
            self,
            swordfish,
            make_mock,
            open_url_kwargs,
            nvme_domain_data
    ):
        _ = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[nvme_domain_data, nvme_domain_data],
            chain_calls=True
        )
        nvme_domain = swordfish.get_nvme_domain('test_domain')
        expected_nvme_domain = NVMeDomain.from_json(swordfish, nvme_domain_data)
        assert nvme_domain.get_name() == expected_nvme_domain.get_name()
        assert nvme_domain.get_id() == expected_nvme_domain.get_id()
        assert nvme_domain.get_description() == expected_nvme_domain.get_description()
        assert nvme_domain.get_path() == expected_nvme_domain.get_path()

    def test_patch_nvme_domain(
            self,
            swordfish,
            make_mock,
            open_url_kwargs,
            nvme_domain_data
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[nvme_domain_data, nvme_domain_data],
            chain_calls=True
        )
        my_data = {
            "Name": "New Name",
            "Description": "New Description",
            "TotalDomainCapacityBytes": 2000,
            "UnallocatedDomainCapacityBytes": 1800
        }
        nvme_domain = swordfish.get_nvme_domain('test_domain')
        nvme_domain.patch_nvme_domain("test_domain", data=my_data)
        expected_url = "https://localhost{0}/NVMeDomains/{1}".format(ROOT_ENDPOINT, "test_domain")
        open_url_kwargs.update(
            method='PATCH',
            url=expected_url,
            data=json.dumps(my_data),
            headers={'Content-Type': 'application/json'}
        )
        open_url_mock.assert_called_with(**open_url_kwargs)
