from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage import Storage

from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import *


class TestCapacitySource:
    def test_create_capacity_source(self, rest, make_mock, storage_ip_attached_drive1, capacity_source_data,
                                    open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC, return_value=capacity_source_data)
        storage = Storage.from_json(rest, storage_ip_attached_drive1)

        volume_id = "SimpleNamespace"
        capacity_source_id = "test-cap"
        body_data = {"Description": "some-description"}

        volume = storage.get_volume(volume_id)
        volume.create_capacity_source(capacity_source_id=capacity_source_id, data=body_data)

        json_data = json.dumps(body_data)
        path = "{0}/CapacitySources/{1}".format(volume.get_path(), capacity_source_id)
        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}".format(path),
            headers={'Content-Type': 'application/json'},
            data=json_data
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_delete_capacity_source(self, rest, make_mock, storage_ip_attached_drive1, capacity_source_data,
                                    open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC, return_value=capacity_source_data)
        storage = Storage.from_json(rest, storage_ip_attached_drive1)

        volume_id = "SimpleNamespace"
        capacity_source_id = "test-cap"

        volume = storage.get_volume(volume_id)
        volume.delete_capacity_source(capacity_source_id)

        path = "{0}/CapacitySources/{1}".format(volume.get_path(), capacity_source_id)
        open_url_kwargs.update(
            method='DELETE',
            url="https://localhost{0}".format(path),
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_patch_capacity_source(self, rest, make_mock, storage_ip_attached_drive1, capacity_source_data,
                                   open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC, return_value=capacity_source_data)
        storage = Storage.from_json(rest, storage_ip_attached_drive1)

        volume_id = "SimpleNamespace"
        capacity_source_id = "test-cap"

        volume = storage.get_volume(volume_id)

        patch_data = {"Description": "some-description"}
        volume.patch_capacity_source(capacity_source_id=capacity_source_id, data=patch_data)

        path = "{0}/CapacitySources/{1}".format(volume.get_path(), capacity_source_id)
        json_patch_data = json.dumps(patch_data)
        open_url_kwargs.update(
            method='PATCH',
            url="https://localhost{0}".format(path),
            headers={'Content-Type': 'application/json'},
            data=json_patch_data
        )
        open_url_mock.assert_called_with(**open_url_kwargs)
