from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import pytest
import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.account.service import AccountService
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import rest, \
    make_mock, account_service, account_data, open_url_kwargs
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.constants import \
    OPEN_URL_FUNC


class TestUser:
    def test_create_user_with_body(self, rest, make_mock, account_service, account_data, open_url_kwargs):
        open_url_mock = make_mock(target=OPEN_URL_FUNC, return_value=account_data)
        service = AccountService.from_json(rest, account_service)
        account_name = "TestUser"
        path = "{0}/{1}".format(service.get_path(), account_name)
        body_data = {"UserName": account_name,
                     "Password": "Password",
                     "RoleId": "CloudAdmin",
                     "Enabled": True}
        json_patch_data = json.dumps(body_data)
        service.create_account(account_name, body_data.get("Password"),
                               body_data.get("RoleId"), body_data.get("Enabled"))
        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}".format(path),
            headers={'Content-Type': 'application/json'},
            data=json_patch_data
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_get_user_which_not_exists(self, rest, make_mock, account_service):
        service = AccountService.from_json(rest, account_service)
        make_mock(
            target=OPEN_URL_FUNC,
            side_effect=RESTClientError
        )
        with pytest.raises(RESTClientError):
            service.get_account("TestUser")
