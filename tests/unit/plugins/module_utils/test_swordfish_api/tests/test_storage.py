from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json
from ansible_collections.spbstu.swordfish.tests.unit.plugins.module_utils.test_swordfish_api.conftest import \
    OPEN_URL_FUNC
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage import Storage
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_pool import StoragePool
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.namespace import Namespace


class TestStorage:

    def test_storage(
            self,
            swordfish,
            make_mock,
            storage_data
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")
        expected_storage = Storage.from_json(swordfish, storage_data)
        assert storage.get_name() == expected_storage.get_name()
        assert storage.get_id() == storage.get_id()
        assert storage.get_description() == expected_storage.get_description()

    def test_get_storage_pool(
            self,
            swordfish,
            make_mock,
            storage_data,
            storage_pools_data):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_pools_data[0]],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")
        storage_pool = storage.get_storage_pool("TestPool")
        expected_storage_pool = StoragePool.from_json(swordfish, storage_pools_data[0])
        assert storage_pool.get_name() == expected_storage_pool.get_name()
        assert storage_pool.get_id() == expected_storage_pool.get_id()
        assert storage_pool.get_description() == expected_storage_pool.get_description()
        assert storage_pool.get_path() == expected_storage_pool.get_path()

    def test_get_storage_pools(
            self,
            swordfish,
            make_mock,
            storage_data,
            storage_pools_collection_data,
            storage_pools_data
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_pools_collection_data, storage_pools_data[0]],
            chain_calls=True
        )
        storage = swordfish.get_storage("IPAttachedDrive1")
        storage_pools = storage.get_storage_pools()
        assert len(storage_pools) == 1
        storage_pool = storage_pools[0]
        expected_storage_pool = StoragePool.from_json(swordfish, storage_pools_data[0])
        assert storage_pool.get_name() == expected_storage_pool.get_name()
        assert storage_pool.get_id() == expected_storage_pool.get_id()
        assert storage_pool.get_description() == expected_storage_pool.get_description()
        assert storage_pool.get_path() == expected_storage_pool.get_path()

    def test_get_namespace(
            self,
            swordfish,
            make_mock,
            storage_data,
            namespace_data
    ):
        make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, namespace_data],
            chain_calls=True
        )
        storage = swordfish.get_storage('IPAttachedDrive1')
        namespace = storage.get_namespace('SimpleNamespace')
        expected_namespace = Namespace.from_json(swordfish, namespace_data)
        assert namespace.get_name() == expected_namespace.get_name()
        assert namespace.get_id() == expected_namespace.get_id()
        assert namespace.get_path() == expected_namespace.get_path()
        assert namespace.get_description() == expected_namespace.get_description()

    def test_delete_storage_pool(
            self,
            swordfish,
            make_mock,
            open_url_kwargs,
            storage_data
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_data],
            chain_calls=True
        )
        storage = swordfish.get_storage('IPAttachedDrive1')
        storage.delete_storage_pool("TestPool")
        open_url_kwargs.update(
            method='DELETE',
            url="https://localhost{0}/StoragePools/{1}".format(storage.get_path(), "TestPool"),
        )
        open_url_mock.assert_called_with(**open_url_kwargs)

    def test_create_storage_pool(
            self,
            swordfish,
            make_mock,
            open_url_kwargs,
            storage_data,
            storage_pools_data
    ):
        open_url_mock = make_mock(
            target=OPEN_URL_FUNC,
            return_value=[storage_data, storage_data],
            chain_calls=True
        )
        storage = swordfish.get_storage('IPAttachedDrive1')
        storage.create_storage_pool(storage_pools_data[0])
        open_url_kwargs.update(
            method='POST',
            url="https://localhost{0}/StoragePools/{1}".format(storage.get_path(), storage_pools_data[0]["Name"]),
            headers={'Content-Type': 'application/json'},
            data=json.dumps(storage_pools_data[0])
        )
        open_url_mock.assert_called_with(**open_url_kwargs)
