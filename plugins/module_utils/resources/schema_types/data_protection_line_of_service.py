from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_array_types import \
    ReplicaTargets, RemoteReplicaTargets
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_bool_types import \
    IsIsolated
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_objects_types import \
    ReplicaAccessLocation, Volumes
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_str_types import \
    MinLifetime
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.status import Status
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.line_of_service import \
    LineOfService

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class DataProtectionLineOfService(LineOfService):
    inner_classes = LineOfService.inner_classes + [IsIsolated, MinLifetime, RemoteReplicaTargets, ReplicaTargets,
                                                   Volumes, Status, ReplicaAccessLocation]
