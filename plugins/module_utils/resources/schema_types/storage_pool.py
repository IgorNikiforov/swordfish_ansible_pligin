from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.capacity import Capacity
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject, SwordfishAPIFieldInt
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_objects_types import \
    AllocatedPools, AllocatedVolumes, ClassesOfService, DefaultClassOfService, Metrics
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_int_types import \
    BlockSizeBytes, MaxBlockSizeBytes, RemainingCapacityPercent, RecoverableCapacitySourceCount
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_array_types import \
    CapacitySources, LowSpaceWarningThresholdPercents
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_bool_types import \
    Compressed, CompressionEnabled, Deduplicated, DeduplicationEnabled, DefaultCompressionBehavior, \
    DefaultDeduplicationBehavior, DefaultEncryptionBehavior, EncryptionEnabled, Encrypted, ReplicationEnabled
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_str_types import \
    Description
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_enum_str_types import PoolType, \
    SupportedPoolTypes, SupportedRAIDTypes, SupportedProvisioningPolicies
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import Name, Id, ODataID, \
    ODataType
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.identifier import Identifier
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.io_statistics import IOStatistics
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.other.end_grp_lifetime import EndGrpLifetime
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.other.nvme_endurance_group_properties import \
    NVMeEnduranceGroupProperties
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.other.nvme_properties import NVMeProperties
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.other.nvme_set_properties import \
    NVMeSetProperties
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.other.storage_pools_links import Links
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.status import Status
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.oem import Oem

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class PredictedMediaLifeLeftPercent(SwordfishAPIFieldInt):
    pass


class StoragePool(SwordfishAPIFieldObject):
    inner_classes = [ODataID, ODataType, AllocatedPools, AllocatedVolumes, BlockSizeBytes, Capacity, CapacitySources,
                     ClassesOfService, Compressed, CompressionEnabled, Deduplicated, DeduplicationEnabled,
                     DefaultClassOfService, DefaultCompressionBehavior, DefaultDeduplicationBehavior,
                     DefaultEncryptionBehavior, Description, Encrypted, EncryptionEnabled, Id, Identifier, IOStatistics,
                     Links, LowSpaceWarningThresholdPercents, MaxBlockSizeBytes, Metrics, Name,
                     NVMeEnduranceGroupProperties, EndGrpLifetime, PredictedMediaLifeLeftPercent, NVMeProperties,
                     NVMeSetProperties, Oem, PoolType, RecoverableCapacitySourceCount, RemainingCapacityPercent,
                     ReplicationEnabled, Status, SupportedPoolTypes, SupportedProvisioningPolicies, SupportedRAIDTypes]
