from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject, SwordfishAPIFieldStr, SwordfishAPIFieldArray
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_str_types import \
    Description
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import Name, Id, ODataID
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.identifier import Identifier
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.oem import Oem

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


# The version describing the creation or last modification of this service option specification.
# The string representing the version shall be in the form: M + ‘.’ + N + ‘.’ + U
# Where: M - The major version (in numeric form). N - The minor version (in numeric form).
# U - The update (e.g. errata or patch in numeric form).
class ClassOfServiceVersion(SwordfishAPIFieldStr):
    pass


# The value shall be a set of data protection service options.
# Within a class of service, one data protection service option shall be present for each replication session.
class DataProtectionLinesOfService(SwordfishAPIFieldArray):
    inner_classes = [ODataID]


# The value shall be a set of data security service options.
class DataSecurityLinesOfService(SwordfishAPIFieldArray):
    inner_classes = [ODataID]


# The value shall be a set of data protection service options.
class DataStorageLinesOfService(SwordfishAPIFieldArray):
    inner_classes = [ODataID]


# The value shall be a set of IO connectivity service options.
# Within a class of service, at most one IO connectivity service option may be present for a value of AccessProtocol.
class IOConnectivityLinesOfService(SwordfishAPIFieldArray):
    inner_classes = [ODataID]


# The value shall be a set of IO performance service options.
class IOPerformanceLinesOfService(SwordfishAPIFieldArray):
    inner_classes = [ODataID]


# This resource shall define a service option composed of one or more line of service entities.
# ITIL defines a service option as a choice of utility or warranty for a service.

class ClassOfService(SwordfishAPIFieldObject):
    inner_classes = [ClassOfServiceVersion, DataProtectionLinesOfService, DataSecurityLinesOfService,
                     DataStorageLinesOfService, Description, Id, Identifier, IOConnectivityLinesOfService,
                     IOPerformanceLinesOfService, Name, Oem]
