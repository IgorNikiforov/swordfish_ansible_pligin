from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishEnumerableString, SwordfishAPIFieldBool
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_int_types import \
    RecoverableCapacitySourceCount
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_enum_str_array_types import \
    AccessCapabilities
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_enum_str_types import \
    ProvisioningPolicy
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.line_of_service import \
    LineOfService

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class IsSpaceEfficient(SwordfishAPIFieldBool):
    pass


class RecoveryTimeObjectives(SwordfishEnumerableString):
    choices = ["Nearline", "Offline", "OnlineActive", "OnlinePassive"]


class DataStorageLineOfService(LineOfService):
    inner_classes = LineOfService.inner_classes + [AccessCapabilities, IsSpaceEfficient, ProvisioningPolicy,
                                                   RecoverableCapacitySourceCount, RecoveryTimeObjectives]
