from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.capacity import Capacity
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_int_types import \
    CapacityBytes, RecoverableCapacitySourceCount, StripSizeBytes
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_array_types import \
    CapacitySources, LowSpaceWarningThresholdPercents, SpareResourceSets
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_bool_types import \
    Compressed, Deduplicated, Encrypted, IOPerfModeEnabled, IsBootCapable, IsShareable, ReplicationEnabled
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_str_types import \
    Description, DisplayName
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_enum_str_types import \
    ProvisioningPolicy, RaidType, ReadCachePolicy, WriteCachePolicy, WriteHoleProtectionPolicy
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_enum_array_types import \
    EncryptionTypes
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.io_statistics import IOStatistics
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import Name, ODataID, \
    ODataType, Id

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class Volume(SwordfishAPIFieldObject):
    inner_classes = [ODataID, ODataType, Name, Id, CapacityBytes, Description, CapacitySources, Capacity, Compressed,
                     Deduplicated, DisplayName, Encrypted, EncryptionTypes, IOPerfModeEnabled, IOStatistics,
                     IsBootCapable, SpareResourceSets, LowSpaceWarningThresholdPercents, IsShareable,
                     ProvisioningPolicy, RaidType, ReadCachePolicy, RecoverableCapacitySourceCount, ReplicationEnabled,
                     StripSizeBytes, WriteCachePolicy, WriteHoleProtectionPolicy]
