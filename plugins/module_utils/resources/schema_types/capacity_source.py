from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_str_types import \
    Description
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_objects_types import \
    ProvidedCapacity, ProvidedClassOfService, ProvidingDrives, ProvidingMemory, ProvidingMemoryChunk, \
    ProvidingPools, ProvidingVolumes
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import Name, Id
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.oem import Oem

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class CapacitySource(SwordfishAPIFieldObject):
    inner_classes = [Description, Id, Name, Oem, ProvidedCapacity, ProvidedClassOfService, ProvidingDrives,
                     ProvidingMemory, ProvidingMemoryChunk, ProvidingPools, ProvidingVolumes]
