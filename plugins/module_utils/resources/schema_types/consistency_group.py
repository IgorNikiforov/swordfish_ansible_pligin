from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject, SwordfishEnumerableString
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_array_types import \
    RemoteReplicaTargets, ReplicaTargets
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_bool_types import \
    IsConsistent
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_objects_types import \
    Volumes
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_str_types import \
    Description
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_enum_str_types import \
    ConsistencyMethod
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.status import Status
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import Name, Id

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class ConsistencyType(SwordfishEnumerableString):
    choices = ["ApplicationConsistent", "CrashConsistent"]
    elements = "str"


class ConsistencyGroup(SwordfishAPIFieldObject):
    inner_classes = [ConsistencyMethod, ConsistencyType, Description, Id, IsConsistent, Name, RemoteReplicaTargets,
                     ReplicaTargets, Volumes, Status]
