from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldInt, \
    SwordfishAPIFieldBool, SwordfishEnumerableString, SwordfishAPIFieldObject, SwordfishAPIFieldStr
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_array_types import \
    CapacitySources, LowSpaceWarningThresholdPercents, ReplicaTargets
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_bool_types import \
    ReplicationEnabled
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_int_types import \
    BlockSizeBytes, RecoverableCapacitySourceCount, RemainingCapacityPercent
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_objects_types import \
    Metrics
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_str_types import \
    Description
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_enum_str_array_types import \
    AccessCapabilities
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import Name, Id, ODataID, \
    ODataType
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.capacity import Capacity
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.identifier import Identifier
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.io_statistics import IOStatistics
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.oem import Oem
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.other.file_systems_links import Links

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class PredictedMediaLifeLeftPercent(SwordfishAPIFieldInt):
    pass


class CasePreserved(SwordfishAPIFieldBool):
    pass


class CaseSensitive(SwordfishAPIFieldBool):
    pass


class CharacterCodeSet(SwordfishEnumerableString):
    choices = ["None", "ASCII", "ExtendedUNIXCode", "ISO2022", "ISO8859_1", "UCS_2", "Unicode", "UTF_16", "UTF_8"]


class ClusterSizeBytes(SwordfishAPIFieldInt):
    pass


class MaxFileNameLengthBytes(SwordfishAPIFieldInt):
    pass


class ExportedShares(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


class ImportedShare(SwordfishAPIFieldStr):
    pass


class ImportedShares(SwordfishAPIFieldObject):
    inner_classes = [ImportedShare]


class RemainingCapacity(SwordfishAPIFieldObject):
    inner_classes = [SwordfishAPIFieldInt]


class ReplicaInfo(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


class FileSystem(SwordfishAPIFieldObject):
    inner_classes = [ODataID, ODataType, AccessCapabilities, BlockSizeBytes, Capacity, CapacitySources, CasePreserved,
                     CaseSensitive, CharacterCodeSet, ClusterSizeBytes, Description, ExportedShares, Id, Identifier,
                     ImportedShares, IOStatistics, Links, LowSpaceWarningThresholdPercents, MaxFileNameLengthBytes,
                     Metrics, Name, Oem, RecoverableCapacitySourceCount, RemainingCapacity, RemainingCapacityPercent,
                     ReplicaInfo, ReplicaTargets, ReplicationEnabled]
