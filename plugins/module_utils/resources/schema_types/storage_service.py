from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_objects_types import \
    ClassesOfService, ClientEndpointGroups, Connections, ConsistencyGroups, DataProtectionLoSCapabilities, \
    DataSecurityLoSCapabilities, DataStorageLoSCapabilities, DefaultClassOfService, Drives, EndpointGroups, Endpoints, \
    FileSystems, IOConnectivityLoSCapabilities, IOPerformanceLoSCapabilities, LinesOfService, Metrics, \
    ServerEndpointGroups, StorageGroups, StoragePools, Volumes
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_array_types \
    import Redundancy, StorageSubsystems, SpareResourceSets
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_str_types import \
    Description
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import Name, Id
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.other.storage_service_links import Links
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.io_statistics import IOStatistics
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.identifier import Identifier
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.status import Status
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.oem import Oem

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


# Collection of resources that the system can make available to one or more host systems.
# The collection can contain: block, file, or object storage;
# local system access points through which the collection is made available;
# hosts, or host access points to which the collection is made available.
class StorageService(SwordfishAPIFieldObject):
    inner_classes = [ClassesOfService, ClientEndpointGroups, Connections, ConsistencyGroups,
                     DataProtectionLoSCapabilities, DataSecurityLoSCapabilities, DataStorageLoSCapabilities,
                     DefaultClassOfService, Description, Drives, EndpointGroups, Endpoints, FileSystems, Id, Identifier,
                     IOConnectivityLoSCapabilities, IOPerformanceLoSCapabilities, IOStatistics, LinesOfService,
                     Links, Metrics, Name, Oem, Redundancy, ServerEndpointGroups, SpareResourceSets, Status,
                     StorageGroups, StoragePools, StorageSubsystems, Volumes]
