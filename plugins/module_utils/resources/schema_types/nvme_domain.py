from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObjectArray, SwordfishAPIFieldObject, SwordfishAPIFieldInt
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_int_types import \
    ANAGroup
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_str_types import \
    Description
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import Id, ODataID, Name
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.oem import Oem
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.status import Status
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.other.nvme_domain_links import Links

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class AvailableFirmwareImages(SwordfishAPIFieldObjectArray):
    inner_classes = [ODataID]


class Controllers(SwordfishAPIFieldObjectArray):
    inner_classes = [ODataID]


class Namespaces(SwordfishAPIFieldObjectArray):
    inner_classes = [ODataID]


class DomainContents(SwordfishAPIFieldObject):
    inner_classes = [Controllers, Namespaces]


class DomainMembers(SwordfishAPIFieldObjectArray):
    inner_classes = [ODataID]


class FirmwareImages(SwordfishAPIFieldObjectArray):
    inner_classes = [ODataID]


class MaximumCapacityPerEnduranceGroupBytes(SwordfishAPIFieldInt):
    pass


class MaxNamespacesSupportedPerController(SwordfishAPIFieldInt):
    pass


class TotalDomainCapacityBytes(SwordfishAPIFieldInt):
    pass


class UnallocatedDomainCapacityBytes(SwordfishAPIFieldInt):
    pass


class NVMEDomain(SwordfishAPIFieldObject):
    inner_classes = [ANAGroup, AvailableFirmwareImages, Description, DomainContents, DomainMembers, FirmwareImages, Id,
                     Links, MaximumCapacityPerEnduranceGroupBytes, MaxNamespacesSupportedPerController, Name, Oem,
                     Status, TotalDomainCapacityBytes, UnallocatedDomainCapacityBytes]
