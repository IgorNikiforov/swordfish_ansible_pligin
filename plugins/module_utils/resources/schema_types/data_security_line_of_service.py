from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject, SwordfishAPIFieldStr, SwordfishEnumerableStringArray, SwordfishEnumerableString
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import ODataID
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.location import Location
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.line_of_service import \
    LineOfService

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class ReplicaAccessLocation(SwordfishAPIFieldObject):
    inner_classes = [Location]


class ReplicaClassOfService(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


class AntivirusEngineProvider(SwordfishAPIFieldStr):
    pass


class AntivirusScanPolicies(SwordfishEnumerableStringArray):
    choices = ["None", "OnFirstRead", "OnPatternUpdate", "OnRename", "OnUpdate"]


class ChannelEncryptionStrength(SwordfishEnumerableString):
    choices = ["Bits_0", "Bits_112", "Bits_128", "Bits_192", "Bits_256"]


class DataSanitizationPolicy(SwordfishEnumerableString):
    choices = ["Clear", "CryptographicErase", "None"]


class HostAuthenticationType(SwordfishEnumerableString):
    choices = ["None", "Password", "PKI", "Ticket"]


class MediaEncryptionStrength(SwordfishEnumerableString):
    choices = ["Bits_0", "Bits_112", "Bits_128", "Bits_192", "Bits_256"]


class SecureChannelProtocol(SwordfishEnumerableString):
    choices = ["IPsec", "None", "RPCSEC_GSS", "TLS"]


class UserAuthenticationType(SwordfishEnumerableString):
    choices = ["None", "Password", "PKI", "Ticket"]


class DataSecurityLineOfService(LineOfService):
    inner_classes = LineOfService.inner_classes + [AntivirusEngineProvider, AntivirusScanPolicies,
                                                   ChannelEncryptionStrength, DataSanitizationPolicy,
                                                   HostAuthenticationType, MediaEncryptionStrength,
                                                   SecureChannelProtocol, UserAuthenticationType]
