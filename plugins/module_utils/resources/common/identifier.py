from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldStr, SwordfishEnumerableString, SwordfishAPIFieldObject

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


# This property shall contain the world-wide unique identifier for the resource.
# The string shall be in the Identifier.DurableNameFormat property value format.
class DurableName(SwordfishAPIFieldStr):
    pass


# This property shall represent the format of the DurableName property.
# For the possible property values, see DurableNameFormat in Property details.
class DurableNameFormat(SwordfishEnumerableString):
    choices = ["EUI", "FC_WWN", "GCXLID", "iQN", "MACAddress", "NAA", "NGUID", "NQN", "NSID", "UUID"]
    elements = "str"


class Identifier(SwordfishAPIFieldObject):
    inner_classes = [DurableName, DurableNameFormat]
