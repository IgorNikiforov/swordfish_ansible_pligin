from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import Name
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject, SwordfishAPIFieldObjectArray
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.io_workload_component import \
    IOWorkloadComponent


__metaclass__ = type


try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class Components(SwordfishAPIFieldObjectArray):
    inner_classes = [IOWorkloadComponent]


class IOWorkload(SwordfishAPIFieldObject):
    inner_classes = [Components, Name]
