from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldBool, SwordfishEnumerableString, SwordfishAPIFieldObject, SwordfishAPIFieldInt, \
    SwordfishAPIFieldStr
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import ODataID
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.data_protection_line_of_service \
    import DataProtectionLineOfService

__metaclass__ = type


try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


# If true, consistency shall be enabled across the source and its associated target replica(s).
# The default value for this property is false.
class ConsistencyEnabled(SwordfishAPIFieldBool):
    pass


# The ConsistencyState enumeration literal shall indicate the current state of consistency.
# For the possible property values, see ConsistencyState in Property details.
class ConsistencyState(SwordfishEnumerableString):
    choices = ["Consistent", "Inconsistent"]


# The ConsistencyStatus enumeration literal shall specify the current status of consistency.
# Consistency may have been disabled or is experiencing an error condition.
# For the possible property values, see ConsistencyStatus in Property details.
class ConsistencyStatus(SwordfishEnumerableString):
    choices = ["Consistent", "Disabled", "InError", "InProgress"]


# The ConsistencyType enumeration literal shall indicate the consistency type used by the source and its associated
# target group. For the possible property values, see ConsistencyType in Property details.
class ConsistencyType(SwordfishEnumerableString):
    choices = ["SequentiallyConsistent"]


# If true, the storage array shall stop receiving data to the source element if copying to a remote element fails.
# The default value for this property is false.
class FailedCopyStopsHostIO(SwordfishAPIFieldBool):
    pass


# Specifies the percent of the work completed to reach synchronization.
# Shall not be instantiated if implementation is not capable of providing this information.
# If related to a group, then PercentSynced shall be an average of the PercentSynced across all members of the group.
class PercentSynced(SwordfishAPIFieldInt):
    pass


# The ReplicaFaultDomain enumeration literal shall describe the fault domain (local or remote) of the replica
# relationship.
class RemoteSourceReplica(SwordfishAPIFieldStr):
    pass


# Deprecated - Use Source Replica. The value shall reference the resource that is the source of this replica.
class Replica(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The ReplicaFaultDomain enumeration literal shall describe the fault domain (local or remote) of the replica
# relationship. For the possible property values, see ReplicaFaultDomain in Property details.
class ReplicaFaultDomain(SwordfishEnumerableString):
    choices = ["Local", "Remote"]


# The enumeration literal shall specify the priority of background copy engine I/O to be managed relative to host I/O
# operations during a sequential background copy operation.
# For the possible property values, see ReplicaPriority in Property details.
class ReplicaPriority(SwordfishEnumerableString):
    choices = ["High", "Low", "Same", "Urgent"]


# The ReplicaProgressStatus enumeration literal shall specify the status of the session with respect to Replication
# activity. For the possible property values, see ReplicaProgressStatus in Property details.
class ReplicaProgressStatus(SwordfishEnumerableString):
    choices = ["Aborting", "Completed", "Detaching", "Dormant", "FailingBack", "FailingOver", "Fracturing",
               "Initializing", "Mixed", "Pending", "Preparing", "RequiresActivate", "RequiresDetach",
               "RequiresFracture", "RequiresResume", "RequiresResync", "RequiresSplit", "Restoring", "Resyncing",
               "Splitting", "Suspending", "Synchronizing", "Terminating"]


# The enumeration literal shall specify whether the source, the target, or both elements are read only to the host.
# For the possible property values, see ReplicaReadOnlyAccess in Property details.
class ReplicaReadOnlyAccess(SwordfishEnumerableString):
    choices = ["Both", "ReplicaElement", "SourceElement"]


# The enumeration literal shall specify whether the copy operation continues after a broken link is restored.
# For the possible property values, see ReplicaRecoveryMode in Property details.
class ReplicaRecoveryMode(SwordfishEnumerableString):
    choices = ["Automatic", "Manual"]


# The ReplicaRole enumeration literal shall represent the source or target role of this replica as known to the
# containing resource. For the possible property values, see ReplicaRole in Property details.
class ReplicaRole(SwordfishEnumerableString):
    choices = ["Source", "Target"]


# Applies to Adaptive mode, and it describes maximum number of bytes the SyncedElement (target) can be out of sync.
# If the number of out-of-sync bytes exceeds the skew value, ReplicaUpdateMode shall be switched to synchronous.
class ReplicaSkewBytes(SwordfishAPIFieldInt):
    pass


# The ReplicaState enumeration literal shall specify the state of the relationship with respect to Replication activity.
# For the possible property values, see ReplicaState in Property details.
class ReplicaState(SwordfishEnumerableString):
    choices = ["Aborted", "Broken", "Failedover", "Fractured", "Inactive", "Initialized", "Invalid", "Mixed",
               "Partitioned", "Prepared", "Restored", "Skewed", "Split", "Suspended", "Synchronized", "Unsynchronized"]


# The ReplicaType enumeration literal shall describe the intended outcome of the replication.
# For the possible property values, see ReplicaType in Property details.
class ReplicaType(SwordfishEnumerableString):
    choices = ["Clone", "Mirror", "Snapshot", "TokenizedClone"]


# The enumeration literal shall specify whether the target elements will be updated synchronously or asynchronously.
# For the possible property values, see ReplicaUpdateMode in Property details.
class ReplicaUpdateMode(SwordfishEnumerableString):
    choices = ["Active", "Adaptive", "Asynchronous", "Synchronous"]


# The last requested or desired state for the relationship.
# The actual state of the relationship shall be represented by ReplicaState.
# When RequestedState reaches the requested state, this property shall be null.
# For the possible property values, see RequestedReplicaState in Property details.
class RequestedReplicaState(SwordfishEnumerableString):
    choices = ["Aborted", "Broken", "Failedover", "Fractured", "Inactive", "Initialized", "Invalid", "Mixed",
               "Partitioned", "Prepared", "Restored", "Skewed", "Split", "Suspended", "Synchronized", "Unsynchronized"]


# The value shall contain the URI to the source replica when located on a different Swordfish service instance.
class SourceReplica(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# If true, Synchronization shall be maintained. The default value for this property is false.
class SyncMaintained(SwordfishAPIFieldBool):
    pass


# The enumeration literal shall specify whether the source, the target, or both elements involved in a copy operation
# are undiscovered. An element is considered undiscovered if its object model is not known to the service performing
# the copy operation. For the possible property values, see UndiscoveredElement in Property details.
class UndiscoveredElement(SwordfishEnumerableString):
    choices = ["ReplicaElement", "SourceElement"]


# The value shall be an ISO 8601 conformant time of day that specifies when the point-in-time copy was taken or when the
# replication relationship is activated, reactivated, resumed or re-established.
# This property shall be null if the implementation is not capable of providing this information.
class WhenActivated(SwordfishAPIFieldStr):
    pass


# The value shall be an ISO 8601 conformant time of day that specifies when the replication relationship is deactivated.
# Do not instantiate this property if implementation is not capable of providing this information.
class WhenDeactivated(SwordfishAPIFieldStr):
    pass


# The value shall be an ISO 8601 conformant time of day that specifies when the replication relationship is established.
# Do not instantiate this property if implementation is not capable of providing this information.
class WhenEstablished(SwordfishAPIFieldStr):
    pass


# The value shall be an ISO 8601 conformant time of day that specifies when the replication relationship is suspended.
# Do not instantiate this property if implementation is not capable of providing this information.
class WhenSuspended(SwordfishAPIFieldStr):
    pass


# The value shall be an ISO 8601 conformant time of day that specifies when the elements were synchronized.
class WhenSynced(SwordfishAPIFieldStr):
    pass


# The value shall be an ISO 8601 conformant time of day that specifies when the replication relationship is
# synchronized. Do not instantiate this property if implementation is not capable of providing this information.
class WhenSynchronized(SwordfishAPIFieldStr):
    pass


# The value shall define the characteristics of a replica.
class ReplicaInfo(SwordfishAPIFieldObject):
    inner_classes = [ConsistencyEnabled, ConsistencyState, ConsistencyStatus, ConsistencyType,
                     DataProtectionLineOfService, FailedCopyStopsHostIO, PercentSynced, RemoteSourceReplica, Replica,
                     ReplicaFaultDomain, ReplicaPriority, ReplicaProgressStatus, ReplicaReadOnlyAccess,
                     ReplicaRecoveryMode, ReplicaRole, ReplicaSkewBytes, ReplicaState, ReplicaType, ReplicaUpdateMode,
                     RequestedReplicaState, SourceReplica, SyncMaintained, UndiscoveredElement, WhenActivated,
                     WhenDeactivated, WhenEstablished, WhenSuspended, WhenSynced, WhenSynchronized]
