from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject, SwordfishAPIFieldInt, SwordfishAPIFieldStr

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class NonIORequests(SwordfishAPIFieldInt):
    @classmethod
    def snake_name(cls):
        return "non_io_requests"


class NonIORequestTime(SwordfishAPIFieldStr):
    @classmethod
    def snake_name(cls):
        return "non_io_request_time"


class ReadHitIORequests(SwordfishAPIFieldInt):
    @classmethod
    def snake_name(cls):
        return "read_hit_io_requests"


class ReadIOKiBytes(SwordfishAPIFieldInt):
    @classmethod
    def snake_name(cls):
        return "read_io_ki_bytes"


class ReadIORequests(SwordfishAPIFieldInt):
    @classmethod
    def snake_name(cls):
        return "read_io_requests"


class ReadIORequestTime(SwordfishAPIFieldStr):
    @classmethod
    def snake_name(cls):
        return "read_io_request_time"


class WriteHitIORequests(SwordfishAPIFieldInt):
    @classmethod
    def snake_name(cls):
        return "write_hit_io_requests"


class WriteIOKiBytes(SwordfishAPIFieldInt):
    @classmethod
    def snake_name(cls):
        return "write_io_ki_bytes"


class WriteIORequests(SwordfishAPIFieldInt):
    @classmethod
    def snake_name(cls):
        return "write_io_requests"


class WriteIORequestTime(SwordfishAPIFieldStr):
    @classmethod
    def snake_name(cls):
        return "write_io_request_time"


class IOStatistics(SwordfishAPIFieldObject):
    inner_classes = [NonIORequests, NonIORequestTime, ReadHitIORequests, ReadIOKiBytes, ReadIORequests,
                     ReadIORequestTime, WriteHitIORequests, WriteIOKiBytes, WriteIORequests, WriteIORequestTime]

    @classmethod
    def snake_name(cls):
        return "io_statistics"
