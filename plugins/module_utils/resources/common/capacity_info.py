from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject, SwordfishAPIFieldInt

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class AllocatedBytes(SwordfishAPIFieldInt):
    pass


class ConsumedBytes(SwordfishAPIFieldInt):
    pass


class ProvisionedBytes(SwordfishAPIFieldInt):
    pass


class GuaranteedBytes(SwordfishAPIFieldInt):
    pass


class CapacityInfo(SwordfishAPIFieldObject):
    inner_classes = [AllocatedBytes, ConsumedBytes, GuaranteedBytes, ProvisionedBytes]
