from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject, SwordfishAPIFieldInt, SwordfishAPIFieldStr, SwordfishEnumerableString
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.schedule import Schedule

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class AverageIOBytes(SwordfishAPIFieldInt):
    pass


class Duration(SwordfishAPIFieldStr):
    pass


class IOAccessPattern(SwordfishEnumerableString):
    choices = ["RandomReadAgain", "RandomReadNew", "ReadWrite", "SequentialRead", "SequentialWrite"]
    elements = "str"


class PercentOfData(SwordfishAPIFieldInt):
    pass


class PercentOfIOPS(SwordfishAPIFieldInt):
    pass


class IOWorkloadComponent(SwordfishAPIFieldObject):
    inner_classes = [AverageIOBytes, Duration, IOAccessPattern, PercentOfData, PercentOfIOPS, Schedule]
