from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_bool_types import \
    IsThinProvisioned
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.capacity_info import CapacityInfo

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class Data(CapacityInfo):
    pass


class Metadata(CapacityInfo):
    pass


class Snapshot(CapacityInfo):
    pass


class Capacity(SwordfishAPIFieldObject):
    inner_classes = [Data, IsThinProvisioned, Metadata, Snapshot]
