from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


# Property names of Oem follow regular expression pattern “^[A-Za-z0-9_]+$”
class Pattern(SwordfishAPIFieldObject):
    inner_classes = []


# This object represents the OEM properties.
# The resource values shall comply with the Redfish Specification-described requirements.
class Oem(SwordfishAPIFieldObject):
    inner_classes = [Pattern]
