from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject, SwordfishAPIFieldStr
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import ODataID

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


# The value shall be the names of the replica.
class ReplicaName(SwordfishAPIFieldStr):
    pass


# The value shall reference a resource to be replicated.
class ReplicaSource(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# A ReplicaRequest shall contain information about the ReplicaSource and the ReplicaName.
class ReplicaRequest(SwordfishAPIFieldObject):
    inner_classes = [ReplicaName, ReplicaSource]
