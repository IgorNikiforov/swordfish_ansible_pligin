from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import ODataID
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject, SwordfishAPIFieldStr, SwordfishAPIFieldArray, SwordfishEnumerableString, \
    SwordfishAPIFieldObjectArray

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class LogEntry(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


class Message(SwordfishAPIFieldStr):
    pass


class MessageArgs(SwordfishAPIFieldArray):
    elements = 'str'


class MessageId(SwordfishAPIFieldStr):
    pass


class OriginOfCondition(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


class Resolution(SwordfishAPIFieldStr):
    pass


class Severity(SwordfishEnumerableString):
    choices = ["Critical", "OK", "Warning"]


class Timestamp(SwordfishAPIFieldStr):
    pass


class Conditions(SwordfishAPIFieldObjectArray):
    inner_classes = [LogEntry, Message, MessageArgs, MessageId, OriginOfCondition, Resolution, Severity, Timestamp]


class Health(SwordfishEnumerableString):
    choices = ["Critical", "OK", "Warning"]


class HealthRollup(SwordfishEnumerableString):
    choices = ["Critical", "OK", "Warning"]


class State(SwordfishEnumerableString):
    choices = ["Absent", "Deferring", "Disabled", "Enabled", "InTest", "Qualified",
               "Quiesced", "StandbyOffline", "StandbySpare", "Starting", "UnavailableOffline",
               "Updating"]


class Status(SwordfishAPIFieldObject):
    inner_classes = [Conditions, Health, HealthRollup, State]
