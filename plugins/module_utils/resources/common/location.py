from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import Name
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishAPIFieldObject, SwordfishAPIFieldInt, SwordfishAPIFieldArray, SwordfishAPIFieldStr, \
    SwordfishEnumerableString
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.oem import Oem

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class AltitudeMeters(SwordfishAPIFieldInt):
    pass


class ContactName(SwordfishAPIFieldStr):
    pass


class EmailAddress(SwordfishAPIFieldStr):
    pass


class PhoneNumber(SwordfishAPIFieldStr):
    pass


class Info(SwordfishAPIFieldStr):
    pass


class InfoFormat(SwordfishAPIFieldStr):
    pass


class Latitude(SwordfishAPIFieldInt):
    pass


class Longitude(SwordfishAPIFieldStr):
    pass


class Contacts(SwordfishAPIFieldArray):
    elements = [ContactName, EmailAddress, PhoneNumber]


class LocationOrdinalValue(SwordfishAPIFieldInt):
    pass


class LocationType(SwordfishEnumerableString):
    choices = ["Backplane", "Bay", "Connector", "Embedded", "Slot", "Socket"]


class Orientation(SwordfishEnumerableString):
    choices = ["BackToFront", "BottomToTop", "FrontToBack", "LeftToRight", "RightToLeft", "TopToBottom"]


class Reference(SwordfishEnumerableString):
    choices = ["Bottom", "Front", "Left", "Middle", "Rear", "Right", "Top"]


class ServiceLabel(SwordfishAPIFieldStr):
    pass


class PartLocationContext(SwordfishAPIFieldStr):
    pass


class City(SwordfishAPIFieldStr):
    pass


class Country(SwordfishAPIFieldStr):
    pass


class ISOCountryCode(SwordfishAPIFieldStr):
    pass


class ISOSubdivisionCode(SwordfishAPIFieldStr):
    pass


class PostalCode(SwordfishAPIFieldStr):
    pass


class StateOrProvince(SwordfishAPIFieldStr):
    pass


class StreetAddress(SwordfishAPIFieldStr):
    pass


class PhysicalAddress(SwordfishAPIFieldObject):
    inner_classes = [City, Country, ISOCountryCode, ISOSubdivisionCode, PostalCode, StateOrProvince, StreetAddress]


class PartLocation(SwordfishAPIFieldObject):
    inner_classes = [LocationOrdinalValue, LocationType, Orientation, Reference, ServiceLabel]


class AdditionalInfo(SwordfishAPIFieldStr):
    pass


class Rack(SwordfishAPIFieldStr):
    pass


class RackOffset(SwordfishAPIFieldInt):
    pass


class RackOffsetUnits(SwordfishEnumerableString):
    choices = ["EIA_310", "OpenU"]


class Row(SwordfishAPIFieldStr):
    pass


class Placement(SwordfishAPIFieldObject):
    inner_classes = [AdditionalInfo, Rack, RackOffset, RackOffsetUnits, Row]


class AdditionalCode(SwordfishAPIFieldStr):
    pass


class Building(SwordfishAPIFieldStr):
    pass


class Community(SwordfishAPIFieldStr):
    pass


class District(SwordfishAPIFieldStr):
    pass


class Division(SwordfishAPIFieldStr):
    pass


class Floor(SwordfishAPIFieldStr):
    pass


class GPSCoords(SwordfishAPIFieldStr):
    pass


class HouseNumber(SwordfishAPIFieldInt):
    pass


class HouseNumberSuffix(SwordfishAPIFieldStr):
    pass


class Landmark(SwordfishAPIFieldStr):
    pass


class LeadingStreetDirection(SwordfishAPIFieldStr):
    pass


class LocationD(SwordfishAPIFieldStr):
    pass


class Neighborhood(SwordfishAPIFieldStr):
    pass


class PlaceType(SwordfishAPIFieldStr):
    pass


class POBox(SwordfishAPIFieldStr):
    pass


class Road(SwordfishAPIFieldStr):
    pass


class RoadBranch(SwordfishAPIFieldStr):
    pass


class RoadPostModifier(SwordfishAPIFieldStr):
    pass


class RoadPreModifier(SwordfishAPIFieldStr):
    pass


class RoadSection(SwordfishAPIFieldStr):
    pass


class RoadSubBranch(SwordfishAPIFieldStr):
    pass


class Room(SwordfishAPIFieldStr):
    pass


class Seat(SwordfishAPIFieldStr):
    pass


class Street(SwordfishAPIFieldStr):
    pass


class StreetSuffix(SwordfishAPIFieldStr):
    pass


class Territory(SwordfishAPIFieldStr):
    pass


class TrailingStreetSuffix(SwordfishAPIFieldStr):
    pass


class Unit(SwordfishAPIFieldStr):
    pass


class PostalAddress(SwordfishAPIFieldObject):
    inner_classes = [AdditionalCode, AdditionalInfo, Building, City, Community, Country, District, Division, Floor,
                     GPSCoords, HouseNumber, HouseNumberSuffix, Landmark, LeadingStreetDirection, LocationD,
                     Name, Neighborhood, PlaceType, POBox, PostalCode, Road, RoadBranch, RoadPostModifier,
                     RoadPreModifier, RoadSection, RoadSubBranch, Room, Seat, Street, StreetSuffix, Territory,
                     TrailingStreetSuffix, Unit]


class Location(SwordfishAPIFieldObject):
    inner_classes = [AltitudeMeters, Contacts, Info, InfoFormat, Latitude, Longitude, PartLocation, Oem,
                     PartLocationContext, PhysicalAddress, Placement]
