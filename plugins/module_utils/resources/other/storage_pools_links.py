from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_objects_types import \
    DefaultClassOfService, OwningStorageResource
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_array_types import \
    SpareResourceSets
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import ODataID
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.oem import Oem

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class DedicatedSpareDrives(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


class Links(SwordfishAPIFieldObject):
    inner_classes = [DedicatedSpareDrives, DefaultClassOfService, Oem, OwningStorageResource, SpareResourceSets]
