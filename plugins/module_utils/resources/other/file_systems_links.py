from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_array_types import \
    SpareResourceSets
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import ODataID
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.oem import Oem

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldObject, \
    SwordfishAPIFieldObjectArray


class ClassOfService(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


class ReplicaCollection(SwordfishAPIFieldObjectArray):
    inner_classes = [ODataID]


class Links(SwordfishAPIFieldObject):
    inner_classes = [ClassOfService, Oem, ReplicaCollection, SpareResourceSets]
