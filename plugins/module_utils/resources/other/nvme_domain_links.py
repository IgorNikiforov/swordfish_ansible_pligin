from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import ODataID
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.oem import Oem

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldObject, \
    SwordfishAPIFieldObjectArray


class AssociatedDomains(SwordfishAPIFieldObjectArray):
    inner_classes = [ODataID]


class Links(SwordfishAPIFieldObject):
    inner_classes = [AssociatedDomains, Oem]
