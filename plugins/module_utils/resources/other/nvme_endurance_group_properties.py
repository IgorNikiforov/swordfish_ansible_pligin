from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.other.end_grp_lifetime import EndGrpLifetime
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.other.nvme_properties import NVMeProperties
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.other.nvme_set_properties import \
    NVMeSetProperties

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class NVMeEnduranceGroupProperties(SwordfishAPIFieldObject):
    inner_classes = [EndGrpLifetime, NVMeProperties, NVMeSetProperties]
