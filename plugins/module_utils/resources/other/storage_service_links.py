from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_objects_types import \
    DataProtectionLoSCapabilities, DataSecurityLoSCapabilities, DataStorageLoSCapabilities, DefaultClassOfService, \
    HostingSystem, IOConnectivityLoSCapabilities, IOPerformanceLoSCapabilities
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.oem import Oem

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


# This property shall contain links to other resources that are related to this resource.
class Links(SwordfishAPIFieldObject):
    inner_classes = [DataProtectionLoSCapabilities, DataSecurityLoSCapabilities, DataStorageLoSCapabilities,
                     DefaultClassOfService, HostingSystem, IOConnectivityLoSCapabilities, IOPerformanceLoSCapabilities,
                     Oem]
