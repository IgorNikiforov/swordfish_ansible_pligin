from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_str_types import \
    EnduranceGroupIdentifier, SetIdentifier
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.swordfish_api_field_int_types import \
    OptimalWriteSizeBytes, Random4kReadTypicalNanoSeconds, UnallocatedNVMNamespaceCapacityBytes

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class NVMeSetProperties(SwordfishAPIFieldObject):
    inner_classes = [EnduranceGroupIdentifier, OptimalWriteSizeBytes, Random4kReadTypicalNanoSeconds, SetIdentifier,
                     UnallocatedNVMNamespaceCapacityBytes]
