from ansible_collections.spbstu.swordfish.plugins.module_utils import endpoints
from ansible_collections.spbstu.swordfish.plugins.module_utils.log import log
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import CaseConverter

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import (
    RESTClientNotFoundError, PathError,
)


def cut_right_part_of_uri(path):
    return "/".join(path.split("/")[:-1])


def get_uri(path, uri=endpoints.ROOT_ENDPOINT):
    key = next((key for key in path if key != 'id'), None)
    if not key:
        return uri
    val = path[key].get('id')
    if val:
        uri = f'{uri}/{CaseConverter.to_pascal_case(key)}/{val}'
    else:
        uri = f'{uri}/{CaseConverter.to_pascal_case(key)}'
    return get_uri(path[key], uri)


def remove_empty_keys(d):
    """Recursively remove all keys with value None from a dictionary."""
    if not isinstance(d, dict):
        return d
    return {
        k: remove_empty_keys(v)
        for k, v in d.items()
        if v is not None
    }


def is_sub_dict(main_dict, sub_dict):
    for key, value in sub_dict.items():
        if key not in main_dict or main_dict[key] != value:
            print("missmatch in volumes:")
            print("key:", key)
            print(f"sub_volume_prop:{value}")
            print(f"main_volume_prop::{main_dict.get(key)}")
            return False
    return True


def mismatch_props(main_dict, sub_dict):
    mismatch_dict = {}
    for key, value in sub_dict.items():
        if key not in main_dict or main_dict[key] != value:
            mismatch_dict[key] = {"old_value": value, "new_value": main_dict.get(key)}
    return mismatch_dict


def is_exist(uri, client):  # type: (str, SwordfishAPI) -> bool
    collection_uri = cut_right_part_of_uri(uri)

    try:
        collection = client.get(collection_uri).json
        members = collection["Members"]
        for member in members:
            member_uri = member["@odata.id"]
            if member_uri == uri:
                return True
        log.append(f'{uri} resource not founded in collection: {collection_uri}')
        log.append("members of collection: " + ", ".join([member["@odata.id"] for member in members]))
        return False
    except RESTClientNotFoundError:
        log.append(f"this collection not exist: {collection_uri})")
        if is_exist(cut_right_part_of_uri(collection_uri), client):
            return False
        raise PathError(log)


def get_resource(uri, client):  # type: (dict, SwordfishAPI) -> None
    return client.get(uri)


def patch_resource(uri, data, client):  # type: (dict, SwordfishAPI) -> None
    return client.patch(uri, body=data)


def create_resource(uri, data, client):  # type: (dict, SwordfishAPI) -> None
    return client.post(uri, body=data)


def delete_resource(uri, client):  # type: (dict, SwordfishAPI) -> None
    return client.delete(uri)
