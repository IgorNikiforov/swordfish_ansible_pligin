from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldStr

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class ODataID(SwordfishAPIFieldStr):
    @classmethod
    def snake_name(cls):
        return "@odata.id"

    @classmethod
    def camel_name(cls):
        return "@odata.id"


class ODataType(SwordfishAPIFieldStr):
    @classmethod
    def snake_name(cls):
        return "@odata.type"

    @classmethod
    def camel_name(cls):
        return "@odata.type"


class Id(SwordfishAPIFieldStr):
    pass


class Name(SwordfishAPIFieldStr):
    pass
