from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldBool

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class Compressed(SwordfishAPIFieldBool):
    pass


class CompressionEnabled(SwordfishAPIFieldBool):
    pass


class Deduplicated(SwordfishAPIFieldBool):
    pass


class DeduplicationEnabled(SwordfishAPIFieldBool):
    pass


class DefaultCompressionBehavior(SwordfishAPIFieldBool):
    pass


class DefaultDeduplicationBehavior(SwordfishAPIFieldBool):
    pass


class DefaultEncryptionBehavior(SwordfishAPIFieldBool):
    pass


class EncryptionEnabled(SwordfishAPIFieldBool):
    pass


class Encrypted(SwordfishAPIFieldBool):
    pass


class IsBootCapable(SwordfishAPIFieldBool):
    pass


class IOPerfModeEnabled(SwordfishAPIFieldBool):
    pass


class IsConsistent(SwordfishAPIFieldBool):
    pass


class IsIsolated(SwordfishAPIFieldBool):
    pass


class IsShareable(SwordfishAPIFieldBool):
    pass


class IsThinProvisioned(SwordfishAPIFieldBool):
    pass


class ReplicationEnabled(SwordfishAPIFieldBool):
    pass
