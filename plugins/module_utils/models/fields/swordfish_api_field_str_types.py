from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldStr

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class Description(SwordfishAPIFieldStr):
    pass


class DisplayName(SwordfishAPIFieldStr):
    pass


class EnduranceGroupIdentifier(SwordfishAPIFieldStr):
    pass


class MinLifetime(SwordfishAPIFieldStr):
    pass


class RecoveryPointObjectiveTime(SwordfishAPIFieldStr):
    pass


class SetIdentifier(SwordfishAPIFieldStr):
    pass
