from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldArray

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class CapacitySources(SwordfishAPIFieldArray):
    elements = "str"


class LowSpaceWarningThresholdPercents(SwordfishAPIFieldArray):
    elements = "str"


class RemoteReplicaTargets(SwordfishAPIFieldArray):
    elements = "str"


# This collection shall contain the redundancy information for the storage subsystem.
class Redundancy(SwordfishAPIFieldArray):
    elements = "str"


class ReplicaTargets(SwordfishAPIFieldArray):
    elements = "str"


class SpareResourceSets(SwordfishAPIFieldArray):
    elements = "str"


# The value shall be a link to a collection of type
# StorageCollection having members that represent storage subsystems managed by this storage service.

class StorageSubsystems(SwordfishAPIFieldArray):
    elements = "str"
