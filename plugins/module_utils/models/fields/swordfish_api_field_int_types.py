from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldInt

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class BlockSizeBytes(SwordfishAPIFieldInt):
    pass


class CapacityBytes(SwordfishAPIFieldInt):
    pass


class DataUnitsRead(SwordfishAPIFieldInt):
    pass


class DataUnitsWritten(SwordfishAPIFieldInt):
    pass


class EnduranceEstimate(SwordfishAPIFieldInt):
    pass


class ErrorInformationLogEntryCount(SwordfishAPIFieldInt):
    pass


class HostReadCommandCount(SwordfishAPIFieldInt):
    pass


class HostWriteCommandCount(SwordfishAPIFieldInt):
    pass


class MaxBlockSizeBytes(SwordfishAPIFieldInt):
    pass


class MediaAndDataIntegrityErrorCount(SwordfishAPIFieldInt):
    pass


class MediaUnitsWritten(SwordfishAPIFieldInt):
    pass


class OptimalWriteSizeBytes(SwordfishAPIFieldInt):
    pass


class PercentUsed(SwordfishAPIFieldInt):
    pass


class PredictedMediaLifeLeftPercent(SwordfishAPIFieldInt):
    pass


class Random4kReadTypicalNanoSeconds(SwordfishAPIFieldInt):
    pass


class RecoverableCapacitySourceCount(SwordfishAPIFieldInt):
    pass


class RemainingCapacityPercent(SwordfishAPIFieldInt):
    pass


class StripSizeBytes(SwordfishAPIFieldInt):
    pass


class UnallocatedNVMNamespaceCapacityBytes(SwordfishAPIFieldInt):
    pass


class ANAGroup(SwordfishAPIFieldInt):
    pass
