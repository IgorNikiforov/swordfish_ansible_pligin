from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishEnumerableStringArray

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class AccessCapabilities(SwordfishEnumerableStringArray):
    choices = ["Append", "Execute", "Read", "Streaming", "Write", "WriteOnce"]
    elements = "str"
