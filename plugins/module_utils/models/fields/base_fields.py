from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import re

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class CaseConverter:
    pattern = re.compile(r'(?<!^)(?=[A-Z])')

    @classmethod
    def snake_name(cls):
        return CaseConverter.pattern.sub('_', cls.__name__).lower()

    @staticmethod
    def to_pascal_case(snake_str):
        components = snake_str.split('_')
        # Capitalize the first letter of each component except the first one,
        # and join them together.
        return ''.join(x.title() for x in components)

    @classmethod
    def camel_name(cls):
        return cls.__name__


class SwordfishAPIFieldBool(CaseConverter):
    def __init__(self, data, *args, **kwargs):  # type: (Dict, list, Dict) -> None
        if not isinstance(data, bool):
            print(self)
            raise TypeError
        self.value = data

    def get_value(self):
        return self.value

    @staticmethod
    def to_spec_arg():
        return {"type": "bool"}

    @classmethod
    def to_spec_dict(cls):
        return {cls.snake_name(): cls.to_spec_arg()}


class SwordfishAPIFieldInt(CaseConverter):
    def __init__(self, data, *args, **kwargs):  # type: (Dict, list, Dict) -> None
        if not isinstance(data, int):
            raise TypeError
        self.value = data

    def get_value(self):
        return self.value

    @staticmethod
    def to_spec_arg():
        return {"type": "int"}

    @classmethod
    def to_spec_dict(cls):
        return {cls.snake_name(): cls.to_spec_arg()}


class SwordfishAPIFieldStr(CaseConverter):
    def __init__(self, data, *args, **kwargs):  # type: (Dict, list, Dict) -> None
        if not isinstance(data, str):
            raise TypeError(data, self)
        self.value = data

    def get_value(self):
        return self.value

    @classmethod
    def to_spec_arg(cls):
        res = {"type": "str"}
        return res

    @classmethod
    def to_spec_dict(cls):
        return {cls.snake_name(): cls.to_spec_arg()}


class SwordfishAPIFieldArray(CaseConverter):
    def __init__(self, data, *args, **kwargs):  # type:(Dict, list, Dict) -> None
        if not isinstance(data, list):
            raise TypeError
        self.value = data

    def get_value(self):
        return self.value

    @classmethod
    def to_spec_arg(cls):
        res = {"type": "list"}
        update_dict = {"elements": cls.elements}
        res.update(update_dict)

        return res

    @classmethod
    def to_spec_dict(cls):
        return {cls.snake_name(): cls.to_spec_arg()}


class SwordfishAPIFieldObject(CaseConverter):

    def __init__(self, data, from_source, *args):  # type: (Dict, str, dict) -> None
        if not isinstance(data, dict):
            raise TypeError(f'{data} must be dict')
        self.repr = {}

        if data is None:
            raise ValueError("data must be not null")
        inner_classes = self.inner_classes
        if from_source == "spec":
            validation_map = {cls.snake_name(): cls for cls in inner_classes}
        else:
            validation_map = {cls.camel_name(): cls for cls in inner_classes}

        for prop, value in data.items():
            if prop in validation_map and value is not None:
                validation_class = validation_map.get(prop)
                val_class_instance = validation_class(value, from_source, validation_map)
                self.repr[validation_class.camel_name()] = val_class_instance.get_value()

    def get_value(self):
        return self.repr

    @classmethod
    def to_spec_arg(cls):
        res = {}
        res["type"] = "dict"
        res["options"] = {}

        for inner_class in cls.inner_classes:
            res["options"][inner_class.snake_name()] = inner_class.to_spec_arg()
        return res

    @classmethod
    def to_spec_dict(cls):
        return {cls.snake_name(): cls.to_spec_arg()}


class SwordfishAPIFieldObjectArray(CaseConverter):

    def __init__(self, data, from_source, *args):  # type: (list, str, dict) -> None
        if not isinstance(data, list):
            raise TypeError(f"{data} must be list")
        self.repr = []
        inner_classes = self.inner_classes
        if from_source == "spec":
            validation_map = {cls.snake_name(): cls for cls in inner_classes}
        else:
            validation_map = {cls.camel_name(): cls for cls in inner_classes}
        if data is None:
            raise ValueError("data must be not null")
        for obj in data:
            el = {}
            for prop, value in obj.items():
                if prop in validation_map and value is not None:
                    validation_class = validation_map.get(prop)
                    val_class_instance = validation_class(value, from_source, validation_map)
                    el[validation_class.camel_name()] = val_class_instance.get_value()
            self.repr.append(el)

    def get_value(self):
        return self.repr

    @classmethod
    def to_spec_arg(cls):
        res = {}
        res["type"] = "list"
        res['elements'] = 'dict'
        res["options"] = {}

        for inner_class in cls.inner_classes:
            res["options"][inner_class.snake_name()] = inner_class.to_spec_arg()
        return res

    @classmethod
    def to_spec_dict(cls):
        return {cls.snake_name(): cls.to_spec_arg()}


class SwordfishEnumerableArray(SwordfishAPIFieldArray):

    @classmethod
    def to_spec_arg(cls):
        res = super().to_spec_arg()
        update_dict = {"elements": cls.elements, "choices": cls.choices}
        res.update(update_dict)
        return res

    @classmethod
    def to_spec_dict(cls):
        return {cls.snake_name(): cls.to_spec_arg()}


class SwordfishEnumerableString(SwordfishAPIFieldStr):

    @classmethod
    def to_spec_arg(cls):
        res = super().to_spec_arg()
        update_dict = {"choices": cls.choices}
        res.update(update_dict)
        return res

    @classmethod
    def to_spec_dict(cls):
        return {cls.snake_name(): cls.to_spec_arg()}


class SwordfishEnumerableStringArray(SwordfishAPIFieldArray):

    @classmethod
    def to_spec_arg(cls):
        res = super().to_spec_arg()
        update_dict = {"elements": cls.elements, "choices": cls.choices}
        res.update(update_dict)
        return res

    @classmethod
    def to_spec_dict(cls):
        return {cls.snake_name(): cls.to_spec_arg()}
