from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.capacity import Capacity
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.common.location import Location
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.universal_props import ODataID
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import SwordfishAPIFieldObject

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class AllocatedPools(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


class AllocatedVolumes(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


class ClassesOfService(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value of each entry in the array shall reference an EndpointGroup.
class ClientEndpointGroups(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value of this property shall contain references to all Connections that include this volume.
class Connections(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value of each entry in the array shall reference a ConsistencyGroup. Contains a link to a resource
class ConsistencyGroups(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value shall reference the data protection capabilities of this service.
# See the DataProtectionLoSCapabilities schema for details on this property.
class DataProtectionLoSCapabilities(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value shall reference the data security capabilities of this service.
# See the DataSecurityLoSCapabilities schema for details on this property.
class DataSecurityLoSCapabilities(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value shall reference the data storage capabilities of this service.
# See the DataStorageLoSCapabilities schema for details on this property.
class DataStorageLoSCapabilities(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# If present, this property shall reference the default class of service for entities allocated by this storage service.
# This default may be overridden by the DefaultClassOfService property values within contained StoragePools.
# See the ClassOfService schema for details on this property.
class DefaultClassOfService(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# A collection that indicates all the drives managed by this storage service.
class Drives(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value of each entry in the array shall reference an EndpointGroup.
class EndpointGroups(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value of each entry in the array shall reference an Endpoint managed by this service.
class Endpoints(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# An array of references to FileSystems managed by this storage service. Contains a link to a resource.
class FileSystems(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value shall reference the ComputerSystem or StorageController that hosts this service.
class HostingSystem(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value shall reference the IO connectivity capabilities of this service.
# See the IOConnectivityLoSCapabilities schema for details on this property.
class IOConnectivityLoSCapabilities(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value shall reference the IO performance capabilities of this service.
# See the IOPerformanceLoSCapabilities schema for details on this property.
class IOPerformanceLoSCapabilities(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value of each entry shall reference a LineOfService collection defined for this service.
class LinesOfService(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# This property shall contain a link to a resource of type StorageServiceMetrics
# that specifies the metrics for this storage service. IO metrics are reported in the IOStatistics property.
# See the StorageServiceMetrics schema for details on this property.
class Metrics(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


class OwningStorageResource(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


class ReplicaAccessLocation(SwordfishAPIFieldObject):
    inner_classes = [Location]


class ReplicaClassOfService(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value of each entry in the array shall reference a EndpointGroup.
class ServerEndpointGroups(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value of each entry in the array shall reference a StorageGroup. Contains a link to a resource.
# Deprecated in v1.6 and later. This property is deprecated in favor of the Connections property.
class StorageGroups(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# An array of references to StoragePools. Contains a link to a resource.

class StoragePools(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# The value shall be the amount of space that has been provided from the ProvidingDrives, ProvidingVolumes,
# ProvidingMemory or ProvidingPools. For property details, see Capacity.
class ProvidedCapacity(Capacity):
    pass


# The value shall reference the provided ClassOfService from the ProvidingDrives, ProvidingVolumes,
# ProvidingMemoryChunks, ProvidingMemory or ProvidingPools. See the ClassOfService schema for details on this property.
class ProvidedClassOfService(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# If present, the value shall be a reference to a contributing drive or drives.
class ProvidingDrives(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# If present, the value shall be a reference to the contributing memory.
class ProvidingMemory(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# If present, the value shall be a reference to the contributing memory chunks.
class ProvidingMemoryChunk(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# If present, the value shall be a reference to a contributing storage pool or storage pools.
# Contains a link to a resource.
class ProvidingPools(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


# If present, the value shall be a reference to a contributing volume or volumes. Contains a link to a resource.
class ProvidingVolumes(SwordfishAPIFieldObject):
    inner_classes = [ODataID]


class Volumes(SwordfishAPIFieldObject):
    inner_classes = [ODataID]
