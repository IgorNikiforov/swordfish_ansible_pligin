from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fields.base_fields import \
    SwordfishEnumerableString

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class NVMePoolType(SwordfishEnumerableString):
    choices = ["EnduranceGroup", "NVMSet"]
    elements = "str"


class PoolType(SwordfishEnumerableString):
    choices = ["Block", "File", "Object", "Pool"]
    elements = "str"


class ProvisioningPolicy(SwordfishEnumerableString):
    choices = ["Fixed", "Thin"]
    elements = "str"


class RaidType(SwordfishEnumerableString):
    choices = ["None", "RAID0", "RAID00", "RAID01", "RAID1", "RAID10",
               "RAID10E", "RAID10Triple", "RAID1E", "RAID1Triple", "RAID3",
               "RAID4", "RAID5", "RAID50", "RAID6", "RAID60", "RAID6TP"]
    elements = "str"


class ReadCachePolicy(SwordfishEnumerableString):
    choices = ["AdaptiveReadAhead", "Off", "ReadAhead"]
    elements = "str"


class RecoveryGeographicObjective(SwordfishEnumerableString):
    choices = ["Datacenter", "Rack", "RackGroup", "Region", "Row", "Server"]
    elements = "str"


class RecoveryTimeObjective(SwordfishEnumerableString):
    choices = ["Nearline", "Offline", "OnlineActive", "OnlinePassive"]
    elements = "str"


class ConsistencyMethod(SwordfishEnumerableString):
    choices = ["HotStandby", "Other", "VASA", "VDI", "VSS"]
    elements = "str"


class SupportedPoolTypes(SwordfishEnumerableString):
    choices = ["Block", "File", "Object", "Pool"]
    elements = "str"


class SupportedProvisioningPolicies(SwordfishEnumerableString):
    choices = ["Fixed", "Thin"]
    elements = "str"


class SupportedRAIDTypes(SwordfishEnumerableString):
    choices = ["None", "RAID0", "RAID00", "RAID01", "RAID1", "RAID10", "RAID10E", "RAID10Triple", "RAID1E",
               "RAID1Triple", "RAID3", "RAID4", "RAID5", "RAID50", "RAID6", "RAID60", "RAID6TP"]
    elements = "str"


class WriteCachePolicy(SwordfishEnumerableString):
    choices = ["Off", "ProtectedWriteBack", "UnprotectedWriteBack", "WriteThrough"]
    elements = "str"


class WriteHoleProtectionPolicy(SwordfishEnumerableString):
    choices = ["DistributedLog", "Journaling", "Oem", "Off"]
    elements = "str"
