from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.capacity_source import CapacitySource
from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import CAPACITY_SOURCES_DATA_ENDPOINT

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None


class Volume(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Volume, self).__init__(*args, **kwargs)

    @property
    def capacity_bytes(self):  # type: () -> int
        return self._get_field("CapacityBytes")

    def set_capacity_bytes(self, new):  # type: (int) -> None
        if not isinstance(new, int):
            raise TypeError("New capacity bytes must be int. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"CapacityBytes": new})
        self.reload()

    @property
    def status(self):  # type: () -> dict
        return self._get_field("Status")

    def set_status(self, new):  # type: (dict) -> None
        if not isinstance(new, dict):
            raise TypeError("New status must be dict. Received: {0}".format(type(new)))
        for i in new.items():
            if not isinstance(i[1], str):
                raise TypeError("New status items must be str. Received: {0}".format(type(i[1])))
        self._client.patch(self._path, body={"Status": new})
        self.reload()

    @property
    def identifiers(self):  # type: () -> Optional[list]
        return self._get_field("Identifiers")

    def set_identifiers(self, new):  # type: (list) -> None
        if not isinstance(new, list):
            raise TypeError("New identifiers must be list. Received: {0}".format(type(new)))
        for i in new:
            if not isinstance(i, str):
                raise TypeError("New identifiers items must be dict. Received: {0}".format(type(i)))
        self._client.patch(self._path, body={"Identifiers": new})
        self.reload()

    @property
    def description(self):  # type: () -> Optional[dict]
        return self._get_field("Description")

    def set_description(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New description must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Description": new})
        self.reload()

    @property
    def raid_type(self):  # type: () -> Optional[str]
        return self._get_field("RAIDType")

    def set_raid_type(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New RAID type must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"RAIDType": new})
        self.reload()

    def get_capacity_sources(self):
        capacity_sources = self._client.get("{0}/CapacitySources".format(self.get_path())).json['Members']
        for capacity_source in capacity_sources:
            capacity_sources.append(CapacitySource(client=self._client, path=capacity_source['@odata.id']))
        return capacity_sources

    def get_capacity_source(self, capacity_source_id):  # type: (str) -> Optional[Volume]
        path = CAPACITY_SOURCES_DATA_ENDPOINT.format(path_to_capacity_source=self.get_path(),
                                                     capacity_source_id=capacity_source_id)
        capacity_source = self._client.get(path).json
        return CapacitySource.from_json(self._client, capacity_source)

    def create_capacity_source(self, capacity_source_id, data):
        path = CAPACITY_SOURCES_DATA_ENDPOINT.format(path_to_capacity_source=self.get_path(),
                                                     capacity_source_id=capacity_source_id)
        self._client.post(path, body=data)

    def patch_capacity_source(self, capacity_source_id, data):
        path = CAPACITY_SOURCES_DATA_ENDPOINT.format(path_to_capacity_source=self.get_path(),
                                                     capacity_source_id=capacity_source_id)
        self._client.patch(path, body=data)

    def delete_capacity_source(self, capacity_source_id):  # type: (str) -> None
        path = CAPACITY_SOURCES_DATA_ENDPOINT.format(path_to_capacity_source=self.get_path(),
                                                     capacity_source_id=capacity_source_id)
        self._client.delete(path)
