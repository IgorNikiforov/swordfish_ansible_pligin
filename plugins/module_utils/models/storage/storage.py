from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import SwordfishFieldNotFoundError, \
    RESTClientNotFoundError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.file_system import FileSystem
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.namespace import Namespace
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.volume import Volume

__metaclass__ = type

try:
    from typing import Optional, ClassVar, List, Dict
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = List = Dict = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import STORAGE_POOLS_ENDPOINT, \
    STORAGE_POOL_DATA_ENDPOINT, FILE_SYSTEM_ENDPOINT, VOLUME_DATA_ENDPOINT
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage_pool import StoragePool


class Storage(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Storage, self).__init__(*args, **kwargs)

    def get_storage_pools(self):  # type: () -> List[StoragePool]
        pools = []
        if not self._data.get("StoragePool"):
            return pools

        storage_pools_data = self._client.get(STORAGE_POOLS_ENDPOINT
                                              .format(path_to_storage=self._path)).json['Members']
        for pool in storage_pools_data:
            pools.append(StoragePool(client=self._client, path=pool['@odata.id']))
        return pools

    def get_storage_pool(self, storage_pool_id):  # type: (str) -> Optional[StoragePool]
        if not isinstance(storage_pool_id, str):
            raise TypeError("Storage_pool_id must be string. Received: {0}".format(type(storage_pool_id)))
        try:
            storage_pool_data = self._client.get(
                STORAGE_POOL_DATA_ENDPOINT.format(path_to_storage=self._path, storage_pool_id=storage_pool_id)).json
            return StoragePool.from_json(self._client, storage_pool_data)
        except RESTClientNotFoundError:
            return None

    def get_namespace(self, namespace_id):  # type: (str) -> Optional[Namespace]
        if not isinstance(namespace_id, str):
            raise TypeError("Namespace_id must be string. Received: {0}".format(type(namespace_id)))
        try:
            namespace_data = self._client.get(
                VOLUME_DATA_ENDPOINT.format(path_to_volume=self._path, volume_id=namespace_id)).json
            return Namespace.from_json(self._client, namespace_data)
        except RESTClientNotFoundError:
            return None

    def delete_storage_pool(self, storage_pool_id):  # type: (str) -> None
        if not isinstance(storage_pool_id, str):
            raise TypeError("Storage_pool_id must be string. Received: {0}".format(type(storage_pool_id)))
        path = STORAGE_POOL_DATA_ENDPOINT.format(path_to_storage=self._path, storage_pool_id=storage_pool_id)
        self._client.delete(path)

    def create_storage_pool(self, data):  # type: (Dict) -> None
        if not isinstance(data, dict):
            raise TypeError("Data must be dict. Received: {0}".format(type(data)))
        if data.get("Name") is None:
            raise SwordfishFieldNotFoundError("Name")
        path = STORAGE_POOL_DATA_ENDPOINT.format(path_to_storage=self._path, storage_pool_id=data["Name"])
        self._client.post(path, body=data)

    def patch_volume(self, volume_id, data):
        path = VOLUME_DATA_ENDPOINT.format(path_to_volume=self._path, volume_id=volume_id)
        return self._client.patch(path, body=data)

    def create_volume(self, volume_id, data):
        path = VOLUME_DATA_ENDPOINT.format(path_to_volume=self._path, volume_id=volume_id)
        if volume_id is None:
            path = "{0}/Volumes".format(self._path)
        return self._client.post(path, body=data)

    def delete_volume(self, volume_id):
        path = VOLUME_DATA_ENDPOINT.format(path_to_volume=self._path, volume_id=volume_id)
        return self._client.delete(path)

    def get_volume(self, volume_id):  # type: (str) -> Volume
        volume_data = self._client.get(VOLUME_DATA_ENDPOINT.format(path_to_volume=self._path, volume_id=volume_id)).json
        return Volume.from_json(self._client, volume_data)

    def get_volumes(self):  # type: () -> List[Volume]
        volumes = []
        if not self._data.get("Volumes"):
            return volumes

        volumes_collection_data = self._client.get("{path}/Volumes"
                                                   .format(path=self._path)).json['Members']

        for volume in volumes_collection_data:
            volumes.append(Volume(client=self._client, path=volume['@odata.id']))
        return volumes_collection_data

    def get_volumes_info(self):  # type: () -> HTTPClientResponse
        volumes_collection_data = self._client.get("{path}/Volumes"
                                                   .format(path=self._path))
        return volumes_collection_data

    def get_file_system(self, file_system_id):  # type: (str) -> Optional[FileSystem]
        if not isinstance(file_system_id, str):
            raise TypeError("File_system_id must be string. Received: {0}".format(type(file_system_id)))
        try:
            file_system_data = self._client.get(
                FILE_SYSTEM_ENDPOINT.format(path_to_file_system=self._path, file_system=file_system_id)).json
            return FileSystem.from_json(self._client, file_system_data)
        except RESTClientNotFoundError:
            return None

    def delete_file_system(self, file_system_id):  # type: (str) -> None
        if not isinstance(file_system_id, str):
            raise TypeError("file_system_id must be string. Received: {0}".format(type(file_system_id)))
        path = FILE_SYSTEM_ENDPOINT.format(path_to_file_system=self._path, file_system=file_system_id)
        self._client.delete(path)

    def create_file_system(self, data):
        if not isinstance(data, dict):
            raise TypeError("Data must be dict. Received: {0}".format(type(data)))
        if data.get("Name") is None:
            raise SwordfishFieldNotFoundError("Name")
        path = FILE_SYSTEM_ENDPOINT.format(path_to_file_system=self._path, file_system=data["Name"])
        self._client.post(path, body=data)

    def patch_file_system(self, file_system_id, data):
        path = FILE_SYSTEM_ENDPOINT.format(path_to_file_system=self._path, file_system=file_system_id)
        return self._client.patch(path, body=data)
