from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

try:
    from typing import Optional, ClassVar
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = ClassVar = None

from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject


class Role(SwordfishAPIObject):

    def __init__(self, *args, **kwargs):
        super(Role, self).__init__(*args, **kwargs)
