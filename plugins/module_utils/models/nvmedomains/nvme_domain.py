from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import ROOT_ENDPOINT
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base import SwordfishAPIObject


class NVMeDomain(SwordfishAPIObject):
    def __init__(self, *args, **kwargs):
        super(NVMeDomain, self).__init__(*args, **kwargs)

    @property
    def actions(self):  # type: () -> list
        return self._get_field("Actions")

    @property
    def description(self):  # type: () -> str
        return self._data.get("Description")

    @description.setter
    def description(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New description must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Description": new})
        self.reload()

    @property
    def id(self):  # type: () -> str
        return self.get_id()

    @property
    def name(self):  # type: () -> str
        return self.get_name()

    @name.setter
    def name(self, new):  # type: (str) -> None
        if not isinstance(new, str):
            raise TypeError("New name must be str. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"Name": new})
        self.reload()

    @property
    def total_domain_capacity_bytes(self):  # type: () -> int
        return self._get_field("TotalDomainCapacityBytes")

    @total_domain_capacity_bytes.setter
    def total_domain_capacity_bytes(self, new):  # type: (int) -> None
        if not isinstance(new, int):
            raise TypeError("New total domain capacity bytes must be int. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"TotalDomainCapacityBytes": new})
        self.reload()

    @property
    def unallocated_domain_capacity_bytes(self):  # type: () -> int
        return self._get_field("UnallocatedDomainCapacityBytes")

    @unallocated_domain_capacity_bytes.setter
    def unallocated_domain_capacity_bytes(self, new):  # type: (int) -> None
        if not isinstance(new, int):
            raise TypeError("New unallocated domain capacity bytes must be int. Received: {0}".format(type(new)))
        self._client.patch(self._path, body={"UnallocatedDomainCapacityBytes": new})
        self.reload()

    def delete_nvme_domain(self, nvme_domain_id):
        if not isinstance(nvme_domain_id, str):
            raise TypeError("NVMeDomains_id must be string. Received: {0}".format(type(nvme_domain_id)))
        path = "{0}/NVMeDomains/{1}".format(ROOT_ENDPOINT, nvme_domain_id)
        self._client.delete(path)

    def patch_nvme_domain(self, nvme_domain_id, data):
        path = "{0}/NVMeDomains/{1}".format(ROOT_ENDPOINT, nvme_domain_id)
        return self._client.patch(path, body=data)
