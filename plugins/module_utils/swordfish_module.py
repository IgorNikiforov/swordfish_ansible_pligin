from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json
from functools import partial

from ansible.module_utils.basic import AnsibleModule

from ansible_collections.spbstu.swordfish.plugins.module_utils import utils
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTAuthorizationError, PathError
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_client import SwordfishAPI


class SwordfishModule(AnsibleModule):

    def __init__(
            self,
            argument_spec=None,
            supports_check_mode=False,
            required_if=None,
            required_one_of=None,
            mutually_exclusive=None,
    ):

        _argument_spec = dict(
            connection=dict(
                required=True,
                type="dict",
                options=dict(
                    base_url=dict(required=True, type="str"),
                    username=dict(required=False, type="str"),
                    password=dict(
                        required=False,
                        type="str",
                        no_log=True,
                    )
                )
            )
        )

        if argument_spec and isinstance(argument_spec, dict):
            _argument_spec.update(argument_spec)
        super(SwordfishModule, self).__init__(
            argument_spec=_argument_spec,
            supports_check_mode=supports_check_mode,
            required_if=required_if,
            required_one_of=required_one_of,
            mutually_exclusive=mutually_exclusive
        )

        connection = self.params['connection']

        self.client = SwordfishAPI(
            base_url=connection['base_url'],
            username=connection['username'],
            password=connection['password']
        )
        try:
            self.client.authorize()
            self.run()
        except RESTAuthorizationError as e:
            self.fail_json(
                msg='Authorization failed',
                error='{0}: {1}'.format(type(e).__name__, e),
            )
        except Exception as e:
            self.fail_json(
                msg='Operation failed',
                error='{0}: {1}'.format(type(e).__name__, e),
            )

    # Вызывается во время запуска модуля.
    def run(self):
        raise NotImplementedError('Method not implemented!')

    def run_module(self, resource_cls):
        action = None
        mismatch_dict = None
        path = utils.remove_empty_keys(self.params["path"])
        target_uri = utils.get_uri(path)
        resp = None
        try:
            resource_exists = utils.is_exist(target_uri, self.client)
        except PathError as e:
            self.fail_json(msg=str(e))

        if self.params['state'] == 'present':
            resource = resource_cls(self.params, from_source="spec")
            if resource_exists:
                server_volume = utils.get_resource(target_uri, self.client)

                server_resource_dict = server_volume.json
                if not utils.is_sub_dict(server_resource_dict, resource.repr):
                    mismatch_dict = utils.mismatch_props(server_resource_dict, resource.repr)
                    action = partial(utils.patch_resource, uri=target_uri, data=resource.repr, client=self.client)
            else:
                action = partial(utils.create_resource, uri=target_uri, data=resource.repr, client=self.client)
        else:  # absent
            if resource_exists:
                action = partial(utils.delete_resource, uri=target_uri, client=self.client)

        if not action:
            self.exit_json(msg='No changes required', changed=False)

        if not self.check_mode:
            resp = action().json

        self.exit_json(
            msg="Operation successful.",
            resp=resp,
            changed=True,
            changes=mismatch_dict
        )

    # Используется для вывода результатов выполнения модуля.
    def exit_json(self, **kwargs):
        self._logout()
        super(SwordfishModule, self).exit_json(**kwargs)

    # Если в процессе выполнения модуля произошло что-то не так, то
    # вызываем fail_json и передаем сообщение, которое будет выведено пользователю.
    def fail_json(self, **kwargs):
        self._logout()
        super(SwordfishModule, self).fail_json(**kwargs)

    def _logout(self):
        try:
            self.client.logout()
        except Exception as e:
            self.warn(
                'Logout failed. {0}: {1}'.format(type(e).__name__, e)
            )
