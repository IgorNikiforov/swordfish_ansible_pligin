from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.data_storage_line_of_service import \
    DataStorageLineOfService as DSLOS
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule

data_storage_lines_of_service = {
    "data_storage_lines_of_service": {
        "type": "dict",
        "options": {
            "id": {"type": "str", "required": True},
        }
    }
}

classes_of_service_spec = {
    "classes_of_service": {
        "type": "dict",
        "options": {
            "id": {"type": "str", "required": True},
            **data_storage_lines_of_service,
        }
    }
}

lines_of_service = {
    "lines_of_service": {
        "type": "dict",
        "options": {
            "id": {"type": "str", "required": True},
            **data_storage_lines_of_service,
        }
    }
}

storage_services_spec = {
    "storage_services": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **lines_of_service,
            **classes_of_service_spec
        },
    },
}

ansible_module_args = {key: value for cls in DSLOS.inner_classes for key, value in cls.to_spec_dict().items()}


class DataStorageLOS(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'state': {
                'type': 'str',
                'default': 'present',
                'choices': ['present', 'absent']
            },
            'path': {
                'type': 'dict',
                "options": {
                    **storage_services_spec
                },
            },
            **ansible_module_args
        }
        super(DataStorageLOS, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )
        self.run()

    def run(self):
        self.run_module(Vol)


def main():
    DataStorageLOS()


if __name__ == "__main__":
    main()
