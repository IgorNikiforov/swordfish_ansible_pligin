from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.storage_service import \
    StorageService as StoSer
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule

storage_services_spec = {
    "storage_services": {
        "type": "dict",
        "options": {
            "id": {"type": "str", "required": True}
        },
    },
}

computer_systems_spec = {
    "computer_systems": {
        "type": "dict",
        "options": {
            "id": {"type": "str", "required": True},
            **storage_services_spec
        },
    },
}

ansible_module_args = {key: value for cls in StoSer.inner_classes for key, value in cls.to_spec_dict().items()}


class StorageService(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'state': {
                'type': 'str',
                'default': 'present',
                'choices': ['present', 'absent']
            },
            'path': {
                'type': 'dict',
                "options": {
                    **computer_systems_spec
                },
            },
            **ansible_module_args
        }
        super(StorageService, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )
        self.run()

    def run(self):
        self.run_module(StoSer)


def main():
    StorageService()


if __name__ == "__main__":
    main()
