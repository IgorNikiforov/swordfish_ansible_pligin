from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

DOCUMENTATION = r"""
"""

RETURN = r"""
"""

EXAMPLES = r"""
"""

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.capacity_source import \
    CapacitySource as CS

capacity_sources_spec = {
    "capacity_sources": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
        },
    },
}

cs_allocated_volumes_spec = {
    "allocated_volumes": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **capacity_sources_spec,
        },
    },
}

cs_storage_pools_spec = {
    "storage_pools": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **cs_allocated_volumes_spec,
            **capacity_sources_spec,
        },
    },
}

cs_file_system_spec = {
    "file_systems": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **capacity_sources_spec,
        },
    },
}

cs_volumes_spec = {
    "volumes": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **capacity_sources_spec,
        },
    },
}

cs_storage_spec = {
    "storage": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **cs_storage_pools_spec,
            **cs_file_system_spec,
            **cs_volumes_spec,
        },
    },
}

cs_systems_spec = {
    "systems": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **cs_storage_spec,
        },
    },
}

cs_storage_services_spec = {
    "storage_services": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **cs_file_system_spec,
            **cs_storage_pools_spec,
            **cs_volumes_spec
        },
    },
}

ansible_module_args = {key: value for cls in CS.inner_classes for key, value in cls.to_spec_dict().items()}


class CapacitySource(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'state': {
                'type': 'str',
                'default': 'present',
                'choices': ['present', 'absent']
            },
            'path': {
                'type': 'dict',
                "options": {
                    **cs_storage_spec,
                    **cs_storage_services_spec,
                    **cs_systems_spec
                },
            },
            **ansible_module_args
        }
        super(CapacitySource, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )
        self.run()

    def run(self):
        self.run_module(CS)


def main():
    CapacitySource()


if __name__ == "__main__":
    main()
