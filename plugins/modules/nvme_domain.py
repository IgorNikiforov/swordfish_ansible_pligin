from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

DOCUMENTATION = r"""
"""

RETURN = r"""
"""

EXAMPLES = r"""
"""

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.nvme_domain import \
    NVMEDomain as ND

nvme_domains_spec = {
    "n_v_me_domains": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
        },
    },
}

ansible_module_args = {key: value for cls in ND.inner_classes for key, value in cls.to_spec_dict().items()}


class NVMEDomain(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'state': {
                'type': 'str',
                'default': 'present',
                'choices': ['present', 'absent']
            },
            'path': {
                'type': 'dict',
                "options": {
                    **nvme_domains_spec,
                },
            },
            **ansible_module_args
        }
        super(NVMEDomain, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )
        self.run()

    def run(self):
        self.run_module(ND)


def main():
    NVMEDomain()


if __name__ == "__main__":
    main()
