from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

DOCUMENTATION = r"""
"""

RETURN = r"""
"""

EXAMPLES = r"""
"""

from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule
from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.file_system import \
    FileSystem as FS

file_systems_spec = {
    "file_systems": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
        },
    },
}

storage_services_spec = {
    "storage_services": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **file_systems_spec
        },
    },
}

storage_spec = {
    "storage": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **file_systems_spec,
        },
    },
}

ansible_module_args = {key: value for cls in FS.inner_classes for key, value in cls.to_spec_dict().items()}


class FileSystem(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'state': {
                'type': 'str',
                'default': 'present',
                'choices': ['present', 'absent']
            },
            'path': {
                'type': 'dict',
                "options": {
                    **storage_spec,
                    **storage_services_spec,
                },
            },
            **ansible_module_args
        }
        super(FileSystem, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )
        self.run()

    def run(self):
        self.run_module(FS)


def main():
    FileSystem()


if __name__ == "__main__":
    main()
