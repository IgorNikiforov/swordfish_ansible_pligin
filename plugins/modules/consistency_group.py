from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.consistency_group import ConsistencyGroup as CG
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule

consistency_group_spec = {
    "consistency_groups": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
        },
    },
}

volumes_spec = {
    "volumes": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **consistency_group_spec

        },
    },
}

storage_spec = {
    "storage": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **consistency_group_spec,
        },
    },
}

storage_services_spec = {
    "storage_services": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **consistency_group_spec,
            **volumes_spec,
        },
    },
}

systems_spec = {
    "systems": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **storage_spec
        },
    },
}

comb = {key: value for cls in CG.inner_classes for key, value in cls.to_spec_dict().items()}


class ConsistencyGroup(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'state': {
                'type': 'str',
                'default': 'present',
                'choices': ['present', 'absent']
            },
            'path': {
                'type': 'dict',
                "options": {
                    **storage_spec,
                    **storage_services_spec,
                    **systems_spec
                },
            },
            **comb,
            # **ConsistencyMethod.to_spec_dict(),
            # **ConsistencyType.to_spec_dict(),
            # **Description.to_spec_dict(),
            # **Id.to_spec_dict(),
            # **IsConsistent.to_spec_dict(),
            # **Name.to_spec_dict(),
            # **RemoteReplicaTargets.to_spec_dict(),
            # **ReplicaTargets.to_spec_dict(),
            # **Volumes.to_spec_dict()

        }
        super(ConsistencyGroup, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )
        self.run()

    def run(self):
        self.run_module(CG)


def main():
    ConsistencyGroup()


if __name__ == "__main__":
    main()
