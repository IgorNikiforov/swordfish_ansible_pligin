from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.class_of_service import \
    ClassOfService as COS
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule

classes_of_service_spec = {
    "classes_of_service": {
        "type": "dict",
        "options": {
            "id": {"type": "str", "required": True},
        }
    }
}

storage_pools_service = {
    "storage_pools": {
        "type": "dict",
        "options": {
            "id": {"type": "str", "required": True},
            **classes_of_service_spec,
        }
    }
}

storage_services_spec = {
    "storage_services": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **storage_pools_service,
            **classes_of_service_spec
        },
    },
}

ansible_module_args = {key: value for cls in COS.inner_classes for key, value in cls.to_spec_dict().items()}


class ClassOfService(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'state': {
                'type': 'str',
                'default': 'present',
                'choices': ['present', 'absent']
            },
            'path': {
                'type': 'dict',
                "options": {
                    **storage_services_spec
                },
            },
            **ansible_module_args
        }
        super(ClassOfService, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )
        self.run()

    def run(self):
        self.run_module(COS)


def main():
    ClassOfService()


if __name__ == "__main__":
    main()
