from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import copy
import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import SwordfishFieldNotFoundError

DOCUMENTATION = r"""
module: storage_pool_file_system
short_description: Create a file system
description:
  - This module supports check mode.
options:
  storage_id:
    type: str
    required: True
  description:
    type: str
    required: False
  name:
    type: str
    required: True
  is_thin_provisioned:
    type: bool
    required: False
  provisioned_bytes:
    type: int
    required: False
  state:
    type: str
    choices: [present, absent]
    default: present
    description:
      - C(present) creates a new file system if I(storage_id) does not exists.
      - C(absent) deletes an existing file system.
"""

RETURN = r"""
msg:
  type: str
  returned: always
  description: Operation status message.
error:
  type: str
  returned: on error
  description: Error details if raised.
"""

EXAMPLES = r"""
- name: Test storage_pool_file_system creation | Create TestFileSystem
  spbstu.swordfish.storage_pool_file_system:
    connection:
        base_url: "{{ base_url }}"
        sername: "Administrator"
        password: "Password"
    description: "file system test for test"
    storage_id: "IPAttachedDrive1"
    provisioned_bytes: 10000
    name: "TestFileSystem"
    is_thin_provisioned: false

- name: Test storage_pool_file_system changing | Change description of TestFileSystem
  spbstu.swordfish.storage_pool_file_system:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    name: "TestFileSystem"
    description: "AnotherDescription"

- name: Test storage_pool_file_system deleting | Delete TestFileSystem
  spbstu.swordfish.storage_pool_file_system:
    connection:
      base_url: "{{ base_url }}"
      username: "Administrator"
      password: "Password"
    storage_id: "IPAttachedDrive1"
    name: "TestFileSystem"
    state: absent
"""

from functools import partial
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule


class StoragePoolFileSystem(SwordfishModule):

    def __init__(self):
        argument_spec = {
            "storage_id": {"type": "str", "required": True},
            "description": {"type": "str", "required": False},
            "name": {"type": "str", "required": True},
            "is_thin_provisioned": {"type": "bool", "required": False},
            "provisioned_bytes": {"type": "int", "required": False},
            "state": {"type": "str", "default": "present", "choices": ["present", "absent"], "required": False},
        }
        super(StoragePoolFileSystem, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True,
        )

    def run(self):
        storage = self.client.get_storage(self.params["storage_id"])
        file_system = storage.get_file_system(self.params["name"])
        action = []
        data = dict()
        if self.params["state"] == "present":

            data["Name"] = self.params["name"]

            if self.params["description"] is not None:
                data["Description"] = self.params["description"]

            if self.params["is_thin_provisioned"] is not None or self.params["provisioned_bytes"] is not None:

                capacity_body = dict()

                if self.params["is_thin_provisioned"] is not None:
                    capacity_body["IsThinProvisioned"] = self.params["is_thin_provisioned"]

                if self.params["provisioned_bytes"] is not None:
                    capacity_data = dict()
                    capacity_data["ProvisionedBytes"] = self.params["provisioned_bytes"]
                    capacity_body["Data"] = capacity_data
                data["Capacity"] = capacity_body

            if file_system:

                if ("Description" in data and
                        (not file_system.contains_field("Description") or
                         file_system.contains_field("Description") and
                         data["Description"] != file_system.description)):
                    action.append(partial(storage.patch_file_system,
                                          self.params["name"],
                                          json.loads(json.dumps(data))))

                if "Capacity" in data:
                    try:
                        old_capacity = copy.deepcopy(file_system.capacity)

                        if ("IsThinProvisioned" in data["Capacity"] and data["Capacity"]["IsThinProvisioned"] !=
                                old_capacity.get("IsThinProvisioned")):
                            old_capacity["IsThinProvisioned"] = data["Capacity"]["IsThinProvisioned"]
                        else:
                            if ("Data" in old_capacity and data["Capacity"]["Data"]["ProvisionedBytes"] !=
                                    old_capacity["Data"].get("ProvisionedBytes")):
                                old_capacity["Data"]["ProvisionedBytes"] = data["Capacity"]["Data"][
                                    "ProvisionedBytes"]
                            else:
                                old_capacity["Data"] = {
                                    "ProvisionedBytes": data["Capacity"]["Data"]["ProvisionedBytes"]
                                }

                        if old_capacity != file_system.capacity:
                            action.append(partial(storage.patch_file_system,
                                                  self.params["name"],
                                                  json.loads(json.dumps(data))))

                    except SwordfishFieldNotFoundError:
                        action.append(partial(storage.patch_file_system,
                                              self.params["name"],
                                              json.loads(json.dumps(data))))
            else:
                action.append(partial(storage.create_file_system, json.loads(json.dumps(data))))
        else:
            if file_system:
                action.append(partial(storage.delete_file_system, self.params["name"]))

        if not self.check_mode:
            for item in action:
                item()
        if action:
            self.exit_json(msg="Operation successful.", changed=True)
        else:
            self.exit_json(msg="No changes required.", changed=False)


def main():
    StoragePoolFileSystem()


if __name__ == "__main__":
    main()
