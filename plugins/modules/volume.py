from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.volume import Volume as Vol
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule

providing_volumes_spec = {
    "providing_volumes": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},

        },
    },
}

allocated_volumes_spec = {
    "allocated_volumes": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},

        },
    },
}

capacity_sources_spec = {
    "capacity_sources": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **providing_volumes_spec
        },
    },
}

volumes_spec = {
    "volumes": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},

        },
    },
}

consistency_group_spec = {
    "consistency_groups": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **volumes_spec
        },
    },
}

file_systems_spec = {
    "file_systems": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **capacity_sources_spec

        },

    },
}

storage_pools_spec = {
    "storage_pools": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **allocated_volumes_spec,
            **capacity_sources_spec,
        },
    },
}

storage_spec = {
    "storage": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **volumes_spec,
            **consistency_group_spec,
            **file_systems_spec,
            **storage_pools_spec,
        },
    },
}

ansible_module_args = {key: value for cls in Vol.inner_classes for key, value in cls.to_spec_dict().items()}


class Volume(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'state': {
                'type': 'str',
                'default': 'present',
                'choices': ['present', 'absent']
            },
            'path': {
                'type': 'dict',
                "options": {
                    **storage_spec
                },
            },
            **ansible_module_args
        }
        super(Volume, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )
        self.run()

    def run(self):
        self.run_module(Vol)


def main():
    Volume()


if __name__ == "__main__":
    main()
