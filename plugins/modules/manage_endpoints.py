#!/usr/bin/env python3
from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from functools import partial

from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import (
    RESTClientNotFoundError,
)
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import RESTClientError
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.base_collection import SwordfishAPICollection
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fabrics.endpoint import Endpoint
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule

DOCUMENTATION = r'''
module: manage_endpoints
short_description: CRUD operation Endpoint
description:
  - CRUD operation Endpoint
options:
  endpoints_collection_path:
    type: str
    required: True
  endpoint_id:
    type: str
    required: True
  state:
    type: str
    choices: [present, absent]
    default: present
    description:
      - C(present) attaches a namespaces if I(namespace) does not attached.
      - C(absent) detached an attached namespace.
'''

EXAMPLES = r'''
'''

RETURN = r'''
'''


class ManageEndpoints(SwordfishModule):
    def __init__(self):
        argument_spec = {
            'endpoints_collection_path': {
                'type': 'str',
                'required': True,
            },
            'endpoint_id': {
                'type': 'str',
                'required': True,
            },
            "state": {
                "type": "str",
                "default": "present",
                "choices": ["present", "absent"],
                "required": False
            },
            "body": {
                "type": "dict",
                "required": False,
                "default": {
                    "Description": "Endpoint",
                    "EndpointProtocol": "NVMeOverFabrics"
                }
            }
        }
        super(ManageEndpoints, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )

    def run(self):
        try:
            endpoints_collection = SwordfishAPICollection.from_json(self.client,
                                                                    self.client.get(self.params["endpoints_collection_path"]).json,
                                                                    Endpoint)
        except Exception as e:
            self.fail_json(
                msg="Operation failed while finding storage controller.",
                error=str(e)
            )
        endpoint_exists = False
        try:
            endpoints_collection.get_resource(self.params["endpoint_id"])
            endpoint_exists = True
        except (RESTClientError, RESTClientNotFoundError) as e:
            pass
        action = None
        if self.params['state'] == 'present':
            if endpoint_exists:
                if self.params['body']:
                    action = partial(endpoints_collection.patch_resource, resource_id=self.params['endpoint_id'],
                                     data=self.params['body'])
            else:
                action = partial(endpoints_collection.create_resource, resource_id=self.params['endpoint_id'],
                                 data=self.params['body'])
        else:  # absent
            if endpoint_exists:
                action = partial(endpoints_collection.delete_resource, resource_id=self.params['endpoint_id'])

        if not action:
            self.exit_json(msg='No changes required', changed=False)

        resp = None
        if not self.check_mode:
            resp = action().json

        self.exit_json(
            msg="Operation successful.",
            resp=resp,
            changed=True,
        )


def main():
    ManageEndpoints()


if __name__ == '__main__':
    main()
