from __future__ import (absolute_import, division, print_function)

from ansible_collections.spbstu.swordfish.plugins.module_utils.resources.schema_types.storage_pool import \
    StoragePool as SP
from ansible_collections.spbstu.swordfish.plugins.module_utils.swordfish_module import SwordfishModule

__metaclass__ = type

DOCUMENTATION = r"""
"""

RETURN = r"""
"""

EXAMPLES = r"""
"""

sp_allocated_pools_spec = {
    "allocated_pools": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
        },
    },
}

sp_providing_pools_spec = {
    "providing_pools": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
        },
    },
}

sp_capacity_sources_spec = {
    "capacity_sources": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **sp_providing_pools_spec
        },
    },
}

sp_storage_pools_spec = {
    "storage_pools": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **sp_allocated_pools_spec,
            **sp_capacity_sources_spec,
        },
    },
}

sp_file_system_spec = {
    "file_systems": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **sp_capacity_sources_spec,
        },
    },
}

sp_volumes_spec = {
    "volumes": {
        "type": 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **sp_allocated_pools_spec,
            **sp_capacity_sources_spec,
        },
    },
}

sp_storage_spec = {
    "storage": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **sp_storage_pools_spec,
            **sp_file_system_spec,
            **sp_volumes_spec,
        },
    },
}

sp_systems_spec = {
    "systems": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **sp_storage_spec,
        },
    },
}

sp_storage_services_spec = {
    "storage_services": {
        'type': 'dict',
        "options": {
            "id": {"type": "str", "required": True},
            **sp_file_system_spec,
            **sp_storage_pools_spec,
            **sp_volumes_spec
        },
    },
}

ansible_module_args = {key: value for cls in SP.inner_classes for key, value in cls.to_spec_dict().items()}


class StoragePool(SwordfishModule):

    def __init__(self):
        argument_spec = {
            'state': {
                'type': 'str',
                'default': 'present',
                'choices': ['present', 'absent']
            },
            'path': {
                'type': 'dict',
                "options": {
                    **sp_storage_spec,
                    **sp_storage_services_spec,
                    **sp_systems_spec
                },
            },
            **ansible_module_args
        }
        super(StoragePool, self).__init__(
            argument_spec=argument_spec,
            supports_check_mode=True
        )
        self.run()

    def run(self):
        self.run_module(SP)


def main():
    StoragePool()


if __name__ == "__main__":
    main()
